#ifndef TABU_H
#define TABU_H
#include <vector>
#include <math.h>
#include "position.h"
#include "path.h"

class Tabu
{
public:
    Tabu();
    static vector<int> TabuSearch(vector<vector<path>>, vector<position>, double&);
    static double dist(position, position);
    static vector<vector<int>> makeNeighborList(vector<int>);
    static bool inTList(vector<vector<int>>, vector<int>);
    static void updateTabuList(vector<vector<int>>&, vector<int>, int);
};

#endif // TABU_H
