#ifndef GA_H
#define GA_H
#include <vector>
#include <math.h>
#include <iomanip>
#include <time.h>
#include <algorithm>
#include <climits>
#include "genome.h"
#include "position.h"
#include "path.h"

using namespace std;

class GA
{
public:
    GA();
    static vector<int> GASearch(double, vector<position>, double&);
    static vector<genome> selection(vector<genome>, int);
    static vector<int> crossover(vector<int>, vector<int>, int, int);
    static void mutation(vector<int>&, int, int);
    static double dist(position, position);
};

#endif // GA_H
