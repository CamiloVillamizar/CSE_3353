#ifndef PATH_H
#define PATH_H
#include <iostream>

using namespace std;

class path
{
private:
    int dest;
    float cost;
    bool visited;
public:
    path();
    path(int);
    path(int, float);
    void setDest(int);
    void setCost(float);
    void setVis(bool);
    int getDest() const;
    float getCost() const;
    bool getVis() const;
    const bool operator> (const path&) const;
    const bool operator< (const path&) const;
    const bool operator== (const path&) const;
    //const void operator =(const path&) const;
    path& operator= (const path&);

    friend ostream& operator<<(ostream& out, const path& p){
        out << "(" << p.dest << ", " << p.cost << ")";
        return out;
    }
};

#endif // PATH_H
