#ifndef ALGOANALYZER_H
#define ALGOANALYZER_H
#include <iostream>
#include <vector>
#include <fstream>
#include <chrono>
#include "position.h"
#include "path.h"
#include "naive.h"
#include "dp.h"
#include "ga.h"
#include "tabu.h"

using namespace std::chrono;

class AlgoAnalyzer
{
public:
    AlgoAnalyzer();
    void run();
private:
    int getCommaNum(string);
    void printList(vector<vector<path>>);
    void getPositions(vector<position>&);
    void printPositions(vector<position>);
    void displayPath(vector<int>);
    void files(int);
};

#endif // ALGOANALYZER_H
