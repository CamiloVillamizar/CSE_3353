#ifndef GENOME_H
#define GENOME_H
#include <vector>
#include <iostream>

using namespace std;

class genome
{
private:
    vector<int> path;
    double fitness;
public:
    genome();
    genome(vector<int>);
    genome(vector<int>, double);
    void setPath(vector<int>);
    void setFitness(double);
    vector<int> getPath();
    double getFitness();
    const bool operator==(const genome&) const;
    const bool operator>(const genome&) const;
    const bool operator<(const genome&) const;

    friend ostream& operator<<(ostream& out, const genome& g){
        for(unsigned int i = 0; i < g.path.size(); i++){
            out << g.path[i] << ", ";
        }
        out << " -> " << g.fitness;
        return out;
    }
};

#endif // GENOME_H
