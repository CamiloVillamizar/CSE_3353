#include "astar.h"

vector<int> AStar::search(vector<vector<path>> adjList, vector<position> posVect, int origin, int dest, int& cost, int& nodesVisited){

    bool* visited = new bool[adjList.size()];//list of visited nodes
    int* sdfo = new int[adjList.size()];//shortest distance from origin
    int* prev = new int[adjList.size()];//contains where the search came from to the current node

    for(int i = 0; i < adjList.size(); i++){
        visited[i] = false;
        sdfo[i] = 999;
        prev[i] = -1;
    }

    sdfo[origin - 1] = 0;

    priority_queue<location*> frontier;
    path current(origin, 0);
    location initLocation(current, 0);
    frontier.push(&initLocation);

    while(current.getDest() != dest){

        current = frontier.top()->getPlace();
        nodesVisited++;
        frontier.pop();
        visited[current.getDest() - 1] = true;

        if(current.getDest() == dest){//if it reached the destination

        }

        vector<path> neighbors = adjList[current.getDest() - 1];

        for(path neighbor : neighbors){//for each neighbor
            if(visited[neighbor.getDest() - 1] == false){//if the neighbor has not been visited
                //cout << "N: " << neighbor.getDest();
                if(sdfo[neighbor.getDest() - 1] > sdfo[current.getDest() - 1] + neighbor.getCost()){//check if the neighbost shortest path is greater than the shortest path to the current + the path's weight
                    //cout << "Resetting sdfo value" << endl;
                    sdfo[neighbor.getDest() - 1] = sdfo[current.getDest() - 1] + neighbor.getCost();//reset shortest distance
                    prev[neighbor.getDest() - 1] = current.getDest();
                    position p1 = posVect[current.getDest() - 1];
                    position p2 = posVect[dest - 1];
                    float distBetPoints = sqrt(pow(p2.getX() - p1.getX(),2) + pow(p2.getY() - p1.getY(),2) + pow(p2.getZ() - p1.getZ(),2));
                    float heuristicVal = sdfo[neighbor.getDest() - 1] + distBetPoints;
                    location* tempLoc = new location(neighbor, heuristicVal);
                    frontier.push(tempLoc);//push to frontier
                }
            }
        }
    }

    cost = sdfo[dest - 1];//set cost to shortest distance from origin to dest
    vector<int> finalPath;//create array for the path
    int curNode = dest;//init curNode to the dest node for traversal

    while(curNode != origin){//until current node reaches the origin again...
        finalPath.push_back(curNode);//add the current node to the path
        curNode = prev[curNode - 1];//and reset current node to the node that came before current
    }

    finalPath.push_back(curNode);

    reverse(finalPath.begin(), finalPath.end());

//    for(int i = 0; i < adjList.size(); i++){
//        delete visited[i];
//        delete sdfo[i];
//        delete prev[i];
//    }

    delete [] visited;
    delete [] sdfo;
    delete [] prev;

    return finalPath;
}
