#include "location.h"

location::location(){
    this->place = path();
    this->cost = 0;
}

location::location(path path){
    this->place = path;
    this->cost = path.getCost();
}

location::location(path path, int cost)
{
    this->place = path;
    this->cost = path.getCost() + cost;
}

void location::setCost(int cost){
    this->cost = cost;
}

path location::getPlace() const{
    return place;
}

int location::getCost() const {
    return cost;
}

const bool location::operator>(const location & arg) const{
    if(cost < arg.cost)
        return true;
    return false;
}

const bool location::operator<(const location & arg) const{
    if(cost > arg.cost)
        return true;
    return false;
}

const bool location::operator==(const location & arg) const{
    if(cost == arg.cost)
        return true;
    return false;
}
