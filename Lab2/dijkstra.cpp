#include "dijkstra.h"

using namespace std;

vector<int> dijkstra::search(vector<vector<path>> aList, vector<position> posVect, int origin, int dest, int& cost, int& nodesVisited){

    vector<vector<path>> adjList = aList;
//    bool visited[16];//test vector to be able to see their contents in the debugger
//    int sdfo[16];
//    int prev[16];

    bool* visited = new bool[aList.size()];//list of visited nodes
    int* sdfo = new int[aList.size()];//shortest distance from origin
    int* prev = new int[aList.size()];//contains where the search came from to the current node

    for(unsigned int i = 0; i < adjList.size(); i++){
        visited[i] = false;
        sdfo[i] = 999;
        prev[i] = -1;
    }

    sdfo[origin - 1] = 0;

    priority_queue<location> frontier;
    path current(origin, 0);
    location initLocation(current, 0);
    frontier.push(initLocation);

    while(current.getDest() != dest){

        //priority_queue<location*> t = frontier;
        current = frontier.top().getPlace();
        nodesVisited++;
        //cout << "Current: " << current.getDest() << endl;
        frontier.pop();
        visited[current.getDest() - 1] = true;

        if(current.getDest() == dest){//if it reached the destination

        }

        vector<path> neighbors = adjList[current.getDest() - 1];

        for(path neighbor : neighbors){//for each neighbor
            if(visited[neighbor.getDest() - 1] == false){//if the neighbor has not been visited
                //cout << "N: " << neighbor.getDest();
                if(sdfo[neighbor.getDest() - 1] > sdfo[current.getDest() - 1] + neighbor.getCost()){//check if the neighbost shortest path is greater than the shortest path to the current + the path's weight
                    //cout << "Resetting sdfo value" << endl;
                    sdfo[neighbor.getDest() - 1] = sdfo[current.getDest() - 1] + neighbor.getCost();//reset shortest distance
                    prev[neighbor.getDest() - 1] = current.getDest();
                    location tempLoc(neighbor, sdfo[neighbor.getDest() - 1]);
                    frontier.push(tempLoc);//push to frontier
                }
            }
        }
    }

    cost = sdfo[dest - 1];//set cost to shortest distance from origin to dest
    vector<int> finalPath;//create array for the path
    int curNode = dest;//init curNode to the dest node for traversal

    while(curNode != origin){//until current node reaches the origin again...
        finalPath.push_back(curNode);//add the current node to the path
        curNode = prev[curNode - 1];//and reset current node to the node that came before current
    }

    finalPath.push_back(curNode);

    reverse(finalPath.begin(), finalPath.end());

    return finalPath;
}
