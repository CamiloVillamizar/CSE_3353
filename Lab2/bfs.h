#ifndef BFS_H
#define BFS_H
#include "path.h"
#include "position.h"
#include <vector>
#include <queue>


using namespace std;

class BFS
{
public:
    static vector<int> search(vector<vector<path>>, vector<position>, int, int, int&, int&);
    static vector<int> searchRec(vector<vector<path>>, vector<position>, int, int, int&, int&);
    static void recursiveSearch(vector<vector<path>>, vector<position>, int, int, int&, int&, bool*, vector<path>, queue<int>, vector<int>&, int, bool, queue<int>, int);
};

#endif // BFS_H
