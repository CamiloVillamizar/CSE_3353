#ifndef ASTAR_H
#define ASTAR_H
#include <vector>
#include "path.h"
#include "position.h"
#include "queue"
#include "location.h"
#include <algorithm>
#include <math.h>


class AStar
{
public:
    static vector<int> search(vector<vector<path>>, vector<position>, int, int, int&, int&);
};

#endif // ASTAR_H
