#ifndef POSITION_H
#define POSITION_H
#include <iostream>

using namespace std;

class position
{
private:
    int x;
    int y;
    int z;
public:
    position();
    position(int, int, int);
    void setX(int);
    void setY(int);
    void setZ(int);
    int getX();
    int getY();
    int getZ();

    friend ostream& operator<<(ostream& out, const position& pos){
        out << "(" << pos.x << ", " << pos.y << ", " << pos.z << ")";
        return out;
    }
};

#endif // POSITION_H
