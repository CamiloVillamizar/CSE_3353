#ifndef DFS_H
#define DFS_H
#include "path.h"
#include "position.h"
#include <vector>
#include <stack>

using namespace std;

class DFS
{
public:
    static vector<int> search(vector<vector<path>>, vector<position>, int, int, int&, int&);
    static vector<int> searchRec(vector<vector<path>>, vector<position>, int, int, int&, int&);
    static void recursiveSearch(vector<vector<path>>, vector<position>, int, int, int&, int&, bool*, vector<path>, stack<int>, vector<int>&, int, bool, stack<int>, int);
};

#endif // DFS_H
