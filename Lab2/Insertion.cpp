#include "Insertion.h"

void Insertion::sort(int* arr, int lowNum, int HighNum, int size)//lowNum and highNum not used in this sorting method
{
	int temp, i, j;
	for(int i = 1; i < size; i++)
	{
		temp = arr[i];
		j = i - 1;
        while (j >= 0 && temp < arr[j])
		{
			arr[j + 1] = arr[j];
            j--;
		}
		arr[j + 1] = temp;
	}
}
