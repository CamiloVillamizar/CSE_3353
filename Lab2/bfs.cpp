#include "bfs.h"

vector<int> BFS::search(vector<vector<path>> aList, vector<position> posVect, int origin, int dest, int& cost, int& nodesVisited){
    cost = 0;

    nodesVisited = 1;//set to 1 for the origin point
    bool* visited = new bool[aList.size()];//create variables needed for the search to work
    vector<path> dests;
    queue<int> q;
    queue<int> costQ;
    int currentCost = 0;
    vector<int> finalPath;

    for(int i = 0; i < aList.size(); i++){//initialize all visited values to false
        visited[i] = false;
    }


    q.push(origin);
    costQ.push(0);
    int nextNode = origin;
    bool firstIter = true;//makes sure the program does affect cost on the first pass

    while(!q.size() == 0){

        if(!visited[q.front() - 1]){//do process if next node has not been visited

            nextNode = q.front();//move to next node in the list of neighbors
            currentCost = costQ.front();

            if(firstIter == false){//skips over null paths for the adj matrix
                while(nextNode == 0){
                    nextNode = q.front();
                    q.pop();
                    currentCost = costQ.front();//skip the cost as well
                    costQ.pop();
                }
            }

            finalPath.push_back(nextNode);//add node to the path

            if(nextNode == dest){//checks if the search is over
                for(int i = 0; i < aList.size(); i++){
                    if(visited[i] == true){
                        nodesVisited++;//if it is, count num of nodes that have been visited
                    }
                }

                while(!costQ.empty()){//set cost value using cost queue
                    cost += costQ.front();
                    costQ.pop();
                }

                return finalPath;
            }
        }
        q.pop();//once the program is done with the node(or its been skipped) pop it and it's cost from the queues
        costQ.pop();

        if(!visited[nextNode - 1]){
            visited[nextNode - 1] = true;//mark the node as visited

            dests = aList[nextNode - 1];//get new destinations list


            for(int i = 0; i < dests.size(); i++){

                if(visited[dests[i].getDest() - 1] == false){
                    q.push(dests[i].getDest());//add any destinations which haven't been visited to the queue
                    costQ.push(dests[i].getCost());//and its cost
                }
            }
        }
        firstIter = false;

    }
}

vector<int> BFS::searchRec(vector<vector<path>> aList, vector<position> posVect, int origin, int dest, int& cost, int& nodesVisited){

    cost = 0;
    nodesVisited = 1;//set to 1 for the origin point
    bool* visited = new bool[aList.size()];//create variables needed for the search to work
    vector<path> dests;
    queue<int> q;
    queue<int> costQ;
    int currentCost = 0;
    vector<int> finalPath;

    for(int i = 0; i < aList.size(); i++){//initialize all visited values to false
        visited[i] = false;
    }



    q.push(origin);//preload the stack to prepare it for the search with the origin
    costQ.push(0);
    int nextNode = origin;
    bool firstIter = true;//makes sure the program does affect cost on the first pass

    recursiveSearch(aList, posVect, origin, dest, cost, nodesVisited, visited, dests, q, finalPath, nextNode, firstIter, costQ, currentCost);

    return finalPath;

//    delete [] visited;
//    visited = nullptr;
}

void BFS::recursiveSearch(vector<vector<path>> aList, vector<position> posVect, int origin, int dest, int& cost, int& nodesVisited, bool* visited, vector<path> dests, queue<int> q, vector<int>& finalPath, int nextNode, bool firstIter, queue<int> costQ, int currentCost){
    if(!visited[q.front() - 1]){//do process if next node has not been visited
        nextNode = q.front();
        currentCost = costQ.front();

        if(firstIter == false){
            while(nextNode == 0){//skips over null paths for the adj matrix
                nextNode = q.front();
                q.pop();
                currentCost = costQ.front();//skip the cost as well
                costQ.pop();
            }
        }

        finalPath.push_back(nextNode);//add node to the path

        if(nextNode == dest){//checks if the search is over
            for(int i = 0; i < aList.size(); i++){
                if(visited[i] == true){
                    nodesVisited++;//if it is, count num of nodes that have been visited
                }
            }

            while(!costQ.empty()){//set cost value using cost queue
                cost += costQ.front();
                costQ.pop();
            }

            return;

        }
    }
    q.pop();//once the program is done with the node(or its been skipped) pop it and it's cost from the queues
    costQ.pop();

    if(!visited[nextNode - 1]){
        visited[nextNode - 1] = true;//mark the node as visited

        dests = aList[nextNode - 1];//get new destinations list


        for(int i = 0; i < dests.size(); i++){

            if(visited[dests[i].getDest() - 1] == false){
                q.push(dests[i].getDest());//add any destinations which haven't been visited to the queue
                costQ.push(dests[i].getCost());//and its cost
            }
        }
    }
    firstIter = false;//after first iteration is complete, set to false for logic

    if(!q.empty()){//make the recursive call
        recursiveSearch(aList, posVect, origin, dest, cost, nodesVisited, visited, dests, q, finalPath, nextNode, firstIter, costQ, currentCost);
    }

}
