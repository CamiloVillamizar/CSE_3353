#include "Insertion.h"
#include "Merge.h"
#include "Sort.h"
#include "Strategy.h"
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "path.h"

using namespace std::chrono;

Strategy::Strategy()
{
    this->size = 10;//set the default size to 10
    this->arr = nullptr;//initialize null values
    this->dataType = "";
    this->subClass = "";
}

void Strategy::load(int size, string dataType, int& origin, int& dest)
{

}

void Strategy::Execute(bool display, int arrSize, int origin, int dest)
{

}

void Strategy::Display()
{

}

void Strategy::Stats(int& pathLength, int& pathCost, double& pathDistance, int& nodeNum, chrono::duration<double>& time)
{

}

void Strategy::Select(string input)
{

}

void Strategy::Save()
{

}

void Strategy::configure()
{
    //for future labs
}

int Strategy::getCommaNum(string input){
    int commaCount = 0;
    for(char c : input){
        if(c == ','){
            commaCount++;
        }
    }
    return commaCount;
}

Strategy::~Strategy()
{
    //delete arr;//this is suppoed to make sure memory doesn't leak,
    //but it makes my program crash and I can't figure out why :/
}
