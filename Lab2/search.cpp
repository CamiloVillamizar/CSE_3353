#include "search.h"

using namespace std::chrono;

Search::Search(){

}

void Search::load(int size, string DT, int& origin, int& dest){
    clearList();

    string fileName = "graph.txt";
//    string fileName = "largeGraph.txt";



    formAdjList(fileName);
    if(DT == "adjMat"){
        formAdjMat();
    }


    addWeights();
    getPositions();


    if(origin == NULL && dest == NULL){
        origin = rand() % adjList.size() + 1;
        dest = rand() % adjList.size() + 1;
    }
}


void Search::formAdjList(string fileName){
    //cout << "Forming List" << endl;
    fstream inFile;
    inFile.open(fileName);
    if(!inFile.is_open()){
        cout << "Graph file did not open" << endl;
    }

    string buffer;
    string strTemp;
    string strCheck;
    int commaNum = 0;
    int tempf;
    float curRow;//keeps track of the current row
    bool rowSet = false;

    while(!inFile.eof()){//loop sets up the tree for connections
        getline(inFile, buffer);
        commaNum = getCommaNum(buffer);
        //cout << commaNum << ": " << buffer << endl;

        if(buffer != ""){
            for(int i = 0; i < commaNum; i++){
                tempf = (int)stof(buffer.substr(0, buffer.find(',')));

                if(rowSet == false){
                    curRow = tempf;
                    vector<path> newVect;
                    adjList.push_back(newVect);
                    rowSet = true;
                } else {
                    path newPath(tempf);
                    adjList[curRow - 1].push_back(newPath);
                }
                buffer = buffer.substr(buffer.find(',') + 1, buffer.length() - buffer.find(',') - 1);
            }

            tempf = (int)stof(buffer);
            adjList[curRow - 1].push_back(tempf);
            rowSet = false;
        }
    }

    inFile.close();
}

void Search::formAdjMat(){

    vector<vector<path>> aList = adjList;

    int height = aList.size();
    int width = 0;

    for(vector<path> vect : adjList){
        for(path p : vect){
            if(p.getDest() > width){
                width = p.getDest();
                break;
            }
        }
        vect.clear();
    }
    adjList.clear();


    path nullPath(NULL);
    vector<path> nullVect;

    while(nullVect.size() < width){
        nullVect.push_back(nullPath);
    }

    while(adjList.size() < height){
        adjList.push_back(nullVect);
    }

    for(int row = 0; row < aList.size(); row++){//i represents the row
        vector<path> vect = aList[row];
        for(int i = 0; i < vect.size(); i++){
            path currentPath = vect[i];
            int column = currentPath.getDest() - 1;//current destination represents the column

            adjList[row][column] = currentPath;


        }
    }


}

void Search::getPositions(){
    ifstream inFile;
    inFile.open("positions.txt");
//    inFile.open("largePositions.txt");
    if(!inFile.is_open()){
        cout << "Positions file did not open" << endl;
    }

    if(!inFile.is_open()){
        cout << "Positions didn't open" << endl;
    }

    string buffer;
    int commaNum = 0;
    int currentNum = 1;
    int x = 0;
    int y = 0;
    int z = 0;
    int tempVal;

    while(!inFile.eof()){
        getline(inFile, buffer);
        commaNum = getCommaNum(buffer);

        if(buffer != ""){
            for(int i = 0; i < commaNum; i++){
                tempVal = stof(buffer.substr(0, buffer.find(',')));

                if(currentNum == 1){
                    currentNum++;
                } else if(currentNum == 2){
                    x = tempVal;
                    currentNum++;
                } else if(currentNum == 3){
                    y = tempVal;
                    currentNum++;
                } else {
                    z = tempVal;
                    currentNum = 1;
                }
                buffer = buffer.substr(buffer.find(',') + 1, buffer.length() - buffer.find(',') - 1);
            }

            tempVal = stof(buffer);
            if(currentNum == 1){
                currentNum++;
            } else if(currentNum == 2){
                x = tempVal;
                currentNum++;
            } else if(currentNum == 3){
                y = tempVal;
                currentNum++;
            } else {
                z = tempVal;
                currentNum = 1;
            }

            position curPos(x, y, z);
            positions.push_back(curPos);
        }
    }
}

void Search::Select(string input){
    if(input == "BFS"){
        funcPtr = BFS::search;
    } else if(input == "BFSRec"){
        funcPtr= BFS::searchRec;
    }else if(input == "DFS"){
        funcPtr = DFS::search;
    } else if(input == "DFSRec"){
        funcPtr = DFS::searchRec;
    }else if(input == "Dijkstra"){
        funcPtr = dijkstra::search;
    } else{
        funcPtr = AStar::search;
    }
    searchType = input;
}

void Search::Execute(bool disp, int arrSize, int origin, int dest){//arrSize used for sort, not used here

    high_resolution_clock::time_point Start = high_resolution_clock::now();

    finalCost = 0;
    nodesVisited = 0;

    finalPath = funcPtr(adjList, positions, origin, dest, finalCost, nodesVisited);

    high_resolution_clock::time_point End = high_resolution_clock::now();
    duration<double> clock  = duration_cast<duration<double>>(End - Start);
    searchTime = clock;

//    if(disp == true){
//        Display();
//    }

}

void Search::Display(){
    for(int node : finalPath){
        if(node == finalPath[finalPath.size() - 1]){
            cout << node << endl;
        } else {
            cout << node << ", ";
        }
    }
}

void Search::printList(){
    vector<path> current;
    for(unsigned int i = 0; i < adjList.size(); i++){
        current = adjList[i];
        cout << i + 1 << " -> ";
        for(path i : current){
            cout << i << ", ";
        }
        cout << endl;
    }
}

void Search::addWeights(){
    ifstream inFile;
    inFile.open("weights.txt");
//    inFile.open("largeWeights.txt");
    if(!inFile.is_open()){
        cout << "Weights file did not open" << endl;
    }

    string buffer;
    int commaNum = 0;
    int origin = 0;
    int dest = 0;
    float weight = 0;
    int currentNum = 1;

    float tempVal;

    while(!inFile.eof()){
        getline(inFile, buffer);
        commaNum = getCommaNum(buffer);

        if(buffer != ""){
            for(int i = 0; i < commaNum; i++){
                tempVal = stof(buffer.substr(0, buffer.find(',')));

                if(currentNum == 1){
                    origin = (int)tempVal;
                    currentNum++;
                } else if(currentNum == 2){
                    dest = (int)tempVal;
                    currentNum++;
                } else {
                    weight = tempVal;
                    currentNum = 1;
                }

                buffer = buffer.substr(buffer.find(',') + 1, buffer.length() - buffer.find(',') - 1);
            }

            tempVal = stof(buffer);

            if(currentNum == 1){
                origin = (int)tempVal;
                currentNum++;
            } else if(currentNum == 2){
                dest = (int)tempVal;
                currentNum++;
            } else {
                weight = tempVal;
                 currentNum = 1;
            }

            for(unsigned int i = 0; i < adjList[origin - 1].size(); i++){
                if(adjList[origin - 1][i].getDest() == dest){
                    adjList[origin - 1][i].setCost(weight);
                }
            }
        }
    }
}

void Search::Stats(int& pathLength, int& pathCost, double& pathDistance, int& nodeNum, chrono::duration<double>& time){
    cout << searchType << " search" << endl;
    cout << "------------------------------" << endl;

    cout << "Path found: " << endl;
    Display();

    pathLength = finalPath.size();
    cout << pathLength << " nodes in the path" << endl;////////////////

    pathCost = finalCost;
    cout << "Final cost of path: " << pathCost << endl;/////////////

    double totalDistance = 0;

    int curr;
    int prev;

    for(int i = 1; i < finalPath.size(); i++){
        prev = finalPath[i - 1];
        curr = finalPath[i];
        position p1 = positions[prev - 1];
        position p2 = positions[curr - 1];
        float distBetPoints = sqrt(pow(p2.getX() - p1.getX(),2) + pow(p2.getY() - p1.getY(),2) + pow(p2.getZ() - p1.getZ(),2));
        totalDistance += distBetPoints;
    }

    pathDistance = totalDistance;
    cout << "Total distance: " << pathDistance << " units" << endl;/////////////

    nodeNum = nodesVisited;
    cout << "Nodes explored: " << nodeNum << endl;////////////////////

    time = searchTime;
    cout << "Search time: " << time.count() << " seconds" << endl;////////////
}

void Search::clearList(){
    for(vector<path> vect : adjList){
        vect.clear();
    }

    adjList.clear();
}

void Search::Save(){

}

//void search::setFuncPtr(string input){

//}

Search::~Search(){

}
