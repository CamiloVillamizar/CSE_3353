#include "position.h"

position::position()
{

}

position::position(int x, int y, int z){
    this->x = x;
    this->y = y;
    this->z = z;
}

void position::setX(int x){
    this->x = x;
}

void position::setY(int y){
    this->y = y;
}

void position::setZ(int z){
    this->z = z;
}

int position::getX(){
    return x;
}

int position::getY(){
    return y;
}

int position::getZ(){
    return z;
}
