#pragma once
#include <string>
#include <time.h>
#include <ctime>//ctime, ratio, and chrono give access to the clock
#include <ratio>
#include <chrono>
#include <vector>

using namespace std;

class Strategy
{

protected:
	int size;
    int* arr;//int array which will be sorted
    string subClass;//keeps track if its searching or sorting
    chrono::duration<double> sortTime;//used to save info from execute to save function
    string type;//type of sort being done (Merge, Insertion, or Bubble)
    string dataType;//type of data being loaded (Random, Reversed... etc)
    //vector<vector<path>> adjList;//adjecency list

    int getCommaNum(string);//private func used by the class
public:
	Strategy();
    virtual void load(int, string, int&, int&) = 0;
    virtual void Execute(bool, int, int, int) = 0;
    virtual void Display() = 0;
    virtual void Select(string) = 0;
    virtual void Stats(int&, int&, double&, int&, chrono::duration<double>&) = 0;
    virtual void Save() = 0;
	void configure();
    //void operator == (Strategy);
	~Strategy();
};

