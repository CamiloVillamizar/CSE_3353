#ifndef LOCATION_H
#define LOCATION_H
#include "path.h"

using namespace std;

class location
{
private:
    path place;
    int cost;//cost from origin to current place
public:
    location();
    location(path);
    location(path, int);
    void setCost(int);
    path getPlace() const;
    int getCost() const;

    const bool operator> (const location&) const;
    const bool operator< (const location&) const;
    const bool operator== (const location&) const;
};

#endif // LOCATION_H
