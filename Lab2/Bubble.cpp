#include "Bubble.h"
#include <string>//gives access to the swap function

void Bubble::sort(int* arr, int lowNum, int highNum, int size)//lowNum and highNum not used in this sort method, its here to make the ptr work
{
	int i, j;
	for (i = 0; i < size - 1; i++) {
		for (j = 0; j < size - i - 1; j++) {
			if (arr[j] > arr[j + 1]) {
                std::swap(arr[j], arr[j + 1]);
			}
		}
	}
}

Bubble::~Bubble()
{
}
