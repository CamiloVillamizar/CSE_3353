#pragma once
#include "Strategy.h"

#include "Merge.h"
#include "Insertion.h"
#include "Bubble.h"

using namespace std;

class Sort : public Strategy
{
private:
    chrono::duration<double> sortTime;//used to save info from execute to save function
    void (*funcPtr)(int*, int, int, int);
    int* arr;
//    int arr[100];
    int arrSize;
    string sortType;
public:
    Sort();
	Sort(int*, int, string);
    void load(int, string, int&, int&);
    void Select(string);
    void Execute(bool, int, int, int);
    void Display();
    void Stats(int&, int&, double&, int&, chrono::duration<double>&);
    void Save();
	~Sort();
};

