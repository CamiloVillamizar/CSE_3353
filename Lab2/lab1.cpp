#include <iostream>
//#include <vector>
//#include <fstream>
//#include <stdlib.h>
//#include <time.h>
#include "Strategy.h"
#include "search.h"
#include "Sort.h"

void generateFiles();
void randFile(int);
void revFile(int);
void unqFile(int);
void rand30File(int);

using namespace std;

int main(int argc, char* argv[])
{
    bool userValues = false;
    int searchOrigin = NULL;
    int searchDest = NULL;

    if(argc > 1){
        searchOrigin = (int)stof(argv[1]);
        searchDest = (int)stof(argv[2]);
        userValues = true;
    }

    srand (time(NULL));

    //    generateFiles();//uncomment to generate new data files

//        int dataSize[] = {10};//Test Arrays
//        string dataTypes[] = {"Random"};
//        string sortTypes[] = {"Merge"};

    Strategy* algo = new Sort;//create algo object

    int dataSize[] = {10, 1000, 10000, 100000};//Real arrays
    string dataTypes[] = {"Random", "Reversed Sorted Order", "20 % unique values", "30 % randomized"};
    string sortTypes[] = {"Merge", "Insertion", "Bubble"};
    bool displayResultsToScreen = false;//set true if you want to see sorted arrays on screen(not recommended for the 1,000 and larger arrays)


    int zero = 0;
    double dZero = (double)0;
    chrono::duration<double> sortTime = (chrono::duration<double>)0;


        for(string currentMethod : sortTypes) {//Iterate through sort algos
            algo->Select(currentMethod);
            cout << "Begining " << currentMethod << " sort analysis" << endl;

            for (int currentSize : dataSize) {//Iterate through array sizes
                cout << endl;
                for (string currentType : dataTypes) {//Iterate through


                    cout << currentMethod << ": " << currentSize << " -> " << currentType << endl;//let the user know which combination of data and algos is being worked on
                    algo->load(currentSize, currentType, zero, zero);//load file & prepare for sorting


                    algo->Execute(displayResultsToScreen, currentSize, zero, zero);//run algos
                    algo->Stats(zero, zero, dZero, zero, sortTime);
                }
            }
            cout << "===========================================================================" << endl;//helps identify when an algo is done
        }

    delete algo;
    algo = nullptr;

    cout << "**********************************************************************************************" << endl;

    string searchTypes[] = {"BFS", "BFSRec", "DFS", "DFSRec", "Dijkstra", "A*"};
    //    string searchTypes[] = {"BFS", "BFSRec"};//test arrays
    string graphs[] = {"adjList", "adjMat"};
    //    string graphs[] = {"adjList"};

    double testResults[2][2][6][5];//array contains results for the 100 trials, dimensions are -> graph type, S -> D/D -> S, searchType, data(length, cost, etc)
    int graphParam = 0;//parameter for graph type in the results array
    int SDParam = 0;//parameter for S -> D/D -> S
    int typeParam = 0;//parameter for search type
    int dataParam = 0;

    for(int a = 0; a < 2; a++){
        for(int b = 0; b < 2; b++){
            for(int c = 0; c < 6; c++){
                for(int d = 0; d < 5; d++){
                    testResults[a][b][c][d] = 0;//prints test results to the screen to look at
                }
            }
        }
    }

    int pathLength = 0;
    int pathCost = 0;
    double pathDistance = 0;
    int nodesExplored = 0;
    chrono::duration<double> searchTime = (chrono::duration<double>)0;

    algo = new Search;

    displayResultsToScreen = true;

    for(int i = 0; i < 100; i++){
        for(string sType : searchTypes){
            for(string graphType : graphs){

                algo->load(0, graphType, searchOrigin, searchDest);//set up the list and search type while also initializing random start and end points for the search
                algo->Select(sType);

                cout << "Executing a " << sType << " search on a " << graphType << endl;
                algo->Execute(displayResultsToScreen, 0, searchOrigin, searchDest);
                algo->Stats(pathLength, pathCost, pathDistance, nodesExplored, searchTime);

                //cout << "pathLength: " << pathLength << "; pathCost: " << pathCost << "; pathDistance: " << pathDistance << "; nodesExplored: " << nodesExplored << "; searchTime: " << searchTime.count() << endl;

                if(graphType == "adjList"){
                    graphParam = 0;
                } else {
                    graphParam = 1;
                }

                SDParam = 0;

                if(sType == "BFS"){
                    typeParam = 0;
                } else if(sType == "BFSRec"){
                    typeParam = 1;
                } else if(sType == "DFS"){
                    typeParam = 2;
                }else if(sType == "DFSRec"){
                    typeParam = 3;
                }else if(sType == "Dijkstra"){
                    typeParam = 4;
                }else {
                    typeParam = 5;
                }

                testResults[graphParam][SDParam][typeParam][0] += pathLength;
                testResults[graphParam][SDParam][typeParam][1] += nodesExplored;
                testResults[graphParam][SDParam][typeParam][2] += searchTime.count();
                testResults[graphParam][SDParam][typeParam][3] += pathDistance;
                testResults[graphParam][SDParam][typeParam][4] += pathCost;


                cout << "\n";

                cout << "Executing the reverse search" << endl;
                algo->Execute(displayResultsToScreen, 0, searchDest, searchOrigin);
                algo->Stats(pathLength, pathCost, pathDistance, nodesExplored, searchTime);

                //cout << "pathLength: " << pathLength << "; pathCost: " << pathCost << "; pathDistance: " << pathDistance << "; nodesExplored: " << nodesExplored << "; searchTime: " << searchTime.count() << endl;

                if(graphType == "adjList"){
                    graphParam = 0;
                } else {
                    graphParam = 1;
                }

                SDParam = 1;

                if(sType == "BFS"){
                    typeParam = 0;
                } else if(sType == "BFSRec"){
                    typeParam = 1;
                } else if(sType == "DFS"){
                    typeParam = 2;
                }else if(sType == "DFSRec"){
                    typeParam = 3;
                }else if(sType == "Dijkstra"){
                    typeParam = 4;
                }else {
                    typeParam = 5;
                }

                testResults[graphParam][SDParam][typeParam][0] += pathLength;
                testResults[graphParam][SDParam][typeParam][1] += nodesExplored;
                testResults[graphParam][SDParam][typeParam][2] += searchTime.count();
                testResults[graphParam][SDParam][typeParam][3] += pathDistance;
                testResults[graphParam][SDParam][typeParam][4] += pathCost;

                cout << "================================\n\n\n" << endl;
            }
        }
        if(userValues == false){
            searchOrigin = NULL;
            searchDest = NULL;
        }
    }

    //double testResults[2][2][6][5];//array contains results for the 100 trials, dimensions are -> graph type, S -> D/D -> S, searchType, data(length, cost, etc)
    //this is a mess :/

    for(int a = 0; a < 2; a++){
        for(int b = 0; b < 2; b++){
            for(int c = 0; c < 6; c++){
                for(int d = 0; d < 5; d++){
                    testResults[a][b][c][d] /= 100;//divide by 100 to get the average
                }
            }
        }
    }

    ofstream outFile;
    outFile.open("programResults.txt");

    string sTypes[] = {"BFS", "BFS Recursive", "DFS", "DFS Recursive", "Dijkstra", "A*"};

    for(int a = 0; a < 2; a++){
        for(int b = 0; b < 2; b++){

            cout << graphs[a] << ": ";
            outFile << graphs[a] << ": ";

            if(b == 0){
                cout << "Source -> Destination" << endl;
                outFile << "Source -> Destination, ";
                for(int i = 0; i < 6; i++){
                    outFile << sTypes[i] << ", ";
                }
                outFile << endl;
            } else {
                cout << "Destination -> Source" << endl;
                outFile << "Destination -> Source, ";
                for(int i = 0; i < 6; i++){
                    outFile << sTypes[i] << ", ";
                }
                outFile << endl;
            }

//            for(int c = 0; c < 6; c++){
//                outFile << "Nodes in path: " << testResults[a][b][c][0] << "; Nodes Explored: " << testResults[a][b][c][1] << "; Execution time: " << testResults[a][b][c][2] << "; Distance: " << testResults[a][b][c][3] << "; Cost: " << testResults[a][b][c][4] << endl;
//            }

            for(int d = 0; d < 5; d++){
                if(d == 0){
                    outFile << "Nodes in Path, " << testResults[a][b][0][d] << ", " << testResults[a][b][1][d] << ", " << testResults[a][b][2][d] << ", " << testResults[a][b][3][d] << ", " << testResults[a][b][4][d] << ", " << testResults[a][b][5][d] << endl;
                } else if(d == 1){
                    outFile << "Nodes Explored, " << testResults[a][b][0][d] << ", " << testResults[a][b][1][d] << ", " << testResults[a][b][2][d] << ", " << testResults[a][b][3][d] << ", " << testResults[a][b][4][d] << ", " << testResults[a][b][5][d] << endl;
                } else if(d == 2){
                    outFile << "Execution time, " << testResults[a][b][0][d] << ", " << testResults[a][b][1][d] << ", " << testResults[a][b][2][d] << ", " << testResults[a][b][3][d] << ", " << testResults[a][b][4][d] << ", " << testResults[a][b][5][d] << endl;
                } else if(d == 3){
                    outFile << "Distance, " << testResults[a][b][0][d] << ", " << testResults[a][b][1][d] << ", " << testResults[a][b][2][d] << ", " << testResults[a][b][3][d] << ", " << testResults[a][b][4][d] << ", " << testResults[a][b][5][d] << endl;
                } else if(d == 4){
                    outFile << "Cost, " << testResults[a][b][0][d] << ", " << testResults[a][b][1][d] << ", " << testResults[a][b][2][d] << ", " << testResults[a][b][3][d] << ", " << testResults[a][b][4][d] << ", " << testResults[a][b][5][d] << endl;
                }
            }


            for(int c = 0; c < 6; c++){

                cout << sTypes[c] << ":" << endl;
                //                //outFile << sTypes[c] << ":" << endl;

                cout << "Nodes in path: " << testResults[a][b][c][0] << "; Nodes Explored: " << testResults[a][b][c][1] << "; Execution time: " << testResults[a][b][c][2] << "; Distance: " << testResults[a][b][c][3] << "; Cost: " << testResults[a][b][c][4] << endl;
                //                //outFile << testResults[a][b][c][0] << ", " << testResults[a][b][c][1] << ", " << testResults[a][b][c][2] << ", " << testResults[a][b][c][3] << ", " << testResults[a][b][c][4] << endl;
                cout << endl;
                //                //outFile << endl;
            }
            cout << "\n\n";
            outFile << "\n\n";
        }
    }

    delete algo;
    algo = nullptr;

    cout << "End Program" << endl;//Makes it easier to see if the whole program executed
    return 0;
}

void generateFiles(){

    int sizes[] = {10, 1000, 10000, 100000};

    for(int size : sizes){
        randFile(size);
        revFile(size);
        unqFile(size);
        rand30File(size);
    }
    cout << "Done Creating files" << endl;
}

void randFile(int size){
    ofstream randomFile;
    if(size == 10)
        randomFile.open("random10.txt");
    else if(size == 1000)
        randomFile.open("random1000.txt");
    else if(size == 10000)
        randomFile.open("random10000.txt");
    else
        randomFile.open("random100000.txt");

    srand(time(NULL));
    for (int i = 0; i < size; i++) {
        randomFile << rand() % size + 1 << endl;
    }
    randomFile.close();
}

void revFile(int size){
    ofstream rev;

    if(size == 10)
        rev.open("revFile10.txt");
    else if(size == 1000)
        rev.open("revFile1000.txt");
    else if(size == 10000)
        rev.open("revFile10000.txt");
    else
        rev.open("revFile100000.txt");


    for (int i = 1; i <= size; i++) {
        rev << size + 1 - i << endl;
    }
    rev.close();
}

void unqFile(int size){
    int arr[] = {1, 2, 3, 4, 5};
    for(int i = 0; i < 25; i++){//makes an array of nums 1->5 in random order
        swap(arr[rand() % 5], arr[rand() % 5]);
    }

    ofstream unq;

    if(size == 10)
        unq.open("unq10.txt");
    else if(size == 1000)
        unq.open("unq1000.txt");
    else if(size == 10000)
        unq.open("unq10000.txt");
    else
        unq.open("unq100000.txt");


    for(int i = 0; i < size; i++){
        if (i < size * .2) {
            unq << arr[0] << endl;
        }
        else if (i < size * .4) {
            unq << arr[1] << endl;
        }
        else if (i < size * .6) {
            unq << arr[2] << endl;
        }
        else if (i < size * .8) {
            unq << arr[3] << endl;
        }
        else {
            unq << arr[4] << endl;
        }
    }
    unq.close();
}

void rand30File(int size){

    ofstream rand30;

    if(size == 10)
        rand30.open("partRand10.txt");
    else if(size == 1000)
        rand30.open("partRand1000.txt");
    else if(size == 10000)
        rand30.open("partRand10000.txt");
    else
        rand30.open("partRand100000.txt");


    int* arr = new int[size];
    for (int i = 0; i < size; i++) {
        arr[i] = i;
    }
    for (int i = 0; i < size * .3; i++) {
        swap(arr[rand() % size], arr[rand() % size]);
    }
    for (int i = 0; i < size; i++){
        rand30 << arr[i] << endl;
    }

    rand30.close();
    delete arr;
    arr = nullptr;
}


