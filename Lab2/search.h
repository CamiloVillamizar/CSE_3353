#ifndef SEARCH_H
#define SEARCH_H
#include <vector>
#include <iostream>
#include <fstream>
#include <ctime>
#include <math.h>
#include "Strategy.h"
#include "path.h"
#include "position.h"
#include "bfs.h"
#include "dfs.h"
#include "dijkstra.h"
#include "astar.h"

using namespace std;

class Search : public Strategy
{
private:
    vector<vector<path>> adjList;
    vector<int> finalPath;
    vector<position> positions;
    int finalCost;
    int nodesVisited;
    chrono::duration<double> searchTime;//used to save info from execute to save function
    vector<int> (*funcPtr)(vector<vector<path>>, vector<position>, int, int, int&, int&);
    string searchType;
public:
    Search();
    void load(int, string, int&, int&);
    void Execute(bool, int, int, int);
    void Display();
    void Select(string);
    void Stats(int&, int&, double&, int&, chrono::duration<double>&);
    void Save();

    void formAdjList(string);
    void formAdjMat();
    void getPositions();
    void printList();
    void clearList();
    void addWeights();
//    void setFuncPtr(string);
    ~Search();
};

#endif // SEARCH_H
