#include "dfs.h"

vector<int> DFS::search(vector<vector<path>> aList, vector<position> posVect, int origin, int dest, int& cost, int& nodesVisited){
    cost = 0;

    nodesVisited = 1;//set to 1 for the origin point
    bool* visited = new bool[aList.size()];//create variables needed for the search to work
    vector<path> dests;
    stack<int> s;
    stack<int> costs;
    int currentCost = 0;
    vector<int> finalPath;

    for(int i = 0; i < aList.size(); i++){//initialize all visited values to false
        visited[i] = false;
    }


    s.push(origin);
    costs.push(0);
    int nextNode = origin;
    bool firstIter = true;//makes sure the program does affect cost on the first pass

    while(!s.size() == 0){

        if(!visited[s.top() - 1]){//do process if next node has not been visited

            nextNode = s.top();
            currentCost = costs.top();

            if(firstIter == false){//skips over null paths for the adj matrix
                while(nextNode == 0){
                    nextNode = s.top();
                    s.pop();
                    currentCost = costs.top();//skip the cost as well
                    costs.pop();
                }
            }

            finalPath.push_back(nextNode);//add node to the path

            if(nextNode == dest){//checks if the search is over
                for(int i = 0; i < aList.size(); i++){
                    if(visited[i] == true){
                        nodesVisited++;//if it is, count num of nodes that have been visited
                    }
                }

                while(!costs.empty()){//set cost value using cost stack
                    cost += costs.top();
                    costs.pop();
                }

                return finalPath;
            }
        }
        s.pop();//once the program is done with the node(or its been skipped) pop it and it's cost from the stacks
        costs.pop();

        if(!visited[nextNode - 1]){
            visited[nextNode - 1] = true;//mark the node as visited

            dests = aList[nextNode - 1];//get new destinations list


            for(int i = 0; i < dests.size(); i++){

                if(visited[dests[i].getDest() - 1] == false){
                    s.push(dests[i].getDest());//add any destinations which haven't been visited to the stack
                    costs.push(dests[i].getCost());//and its cost
                }
            }
        }
        firstIter = false;

    }
}

vector<int> DFS::searchRec(vector<vector<path>> aList, vector<position> posVect, int origin, int dest, int& cost, int& nodesVisited){

    cost = 0;
    nodesVisited = 1;//set to 1 for the origin point
    bool* visited = new bool[aList.size()];//create variables needed for the search to work
    vector<path> dests;
    stack<int> s;
    stack<int> costs;
    int currentCost = 0;
    vector<int> finalPath;

    for(unsigned int i = 0; i < aList.size(); i++){//initialize all visited values to false
        visited[i] = false;
    }

    s.push(origin);//preload the stack to prepare it for the search with the origin
    costs.push(0);
    int nextNode = origin;
    bool firstIter = true;//makes sure the program does affect cost on the first pass

    recursiveSearch(aList, posVect, origin, dest, cost, nodesVisited, visited, dests, s, finalPath, nextNode, firstIter, costs, currentCost);


    return finalPath;
}

void DFS::recursiveSearch(vector<vector<path>> aList, vector<position> posVect, int origin, int dest, int& cost, int& nodesVisited, bool* visited, vector<path> dests, stack<int> s, vector<int>& finalPath, int nextNode, bool firstIter, stack<int> costs, int currentCost){
    if(!visited[s.top() - 1]){//do process if next node has not been visited
        nextNode = s.top();
        currentCost = costs.top();

        if(firstIter == false){
            while(nextNode == 0){//skips over null paths for the adj matrix
                nextNode = s.top();
                s.pop();
                currentCost = costs.top();//skip the cost as well
                costs.pop();
            }
        }

        finalPath.push_back(nextNode);//add node to the path

        if(nextNode == dest){//checks if the search is over
            for(unsigned int i = 0; i < aList.size(); i++){
                if(visited[i] == true){
                    nodesVisited++;//if it is, count num of nodes that have been visited
                }
            }

            while(!costs.empty()){//set cost value using cost stack
                cost += costs.top();
                costs.pop();
            }

            return;

        }
    }
    s.pop();//once the program is done with the node(or its been skipped) pop it and it's cost from the stacks
    costs.pop();

    if(!visited[nextNode - 1]){
        visited[nextNode - 1] = true;//mark the node as visited

        dests = aList[nextNode - 1];//get new destinations list


        for(int i = 0; i < dests.size(); i++){

            if(visited[dests[i].getDest() - 1] == false){
                s.push(dests[i].getDest());//add any destinations which haven't been visited to the stack
                costs.push(dests[i].getCost());//and its cost
            }
        }
    }
    firstIter = false;//after first iteration is complete, set to false for logic

    if(!s.empty()){//make the recursive call
        recursiveSearch(aList, posVect, origin, dest, cost, nodesVisited, visited, dests, s, finalPath, nextNode, firstIter, costs, currentCost);
    }

}
