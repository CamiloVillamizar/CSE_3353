#include "Sort.h"
#include "Insertion.h"
#include "Merge.h"
#include "Bubble.h"
#include <iostream>
#include <fstream>

using namespace std::chrono;

Sort::Sort(){

}

Sort::Sort(int* arr, int size, string type)
{

}

void Sort::load(int size, string dataType, int& origin, int& dest){//origin and dest not used here, theyre for search
    this->size = size;
    this->arr = new int[size];
    this->dataType = dataType;

    ifstream inFile;//Creates a input stream object which will
    //be chosen below depending on what the current test is

    if (dataType == "Random") {
        if(size == 10)
            inFile.open("random10.txt", ifstream::in);
        else if(size == 1000)
            inFile.open("random1000.txt", ifstream::in);
        else if(size == 10000)
            inFile.open("random10000.txt", ifstream::in);
        else
            inFile.open("random100000.txt", ifstream::in);
    }

    else if (dataType == "Reversed Sorted Order") {
        if(size == 10)
            inFile.open("revFile10.txt", ifstream::in);
        else if(size == 1000)
            inFile.open("revFile1000.txt", ifstream::in);
        else if(size == 10000)
            inFile.open("revFile10000.txt", ifstream::in);
        else
            inFile.open("revFile100000.txt", ifstream::in);
    }

    else if (dataType == "20 % unique values") {
        if(size == 10)
            inFile.open("unq10.txt", ifstream::in);
        else if(size == 1000)
            inFile.open("unq1000.txt", ifstream::in);
        else if(size == 10000)
            inFile.open("unq10000.txt", ifstream::in);
        else
            inFile.open("unq100000.txt", ifstream::in);
    }

    else if (dataType == "30 % randomized") {
        if(size == 10)
            inFile.open("partRand10.txt", ifstream::in);
        else if(size == 1000)
            inFile.open("partRand1000.txt", ifstream::in);
        else if(size == 10000)
            inFile.open("partRand10000.txt", ifstream::in);
        else
            inFile.open("partRand100000.txt", ifstream::in);
    }

    for(int i = 0; i < size; i++){//loop pulls data in from the file into the array
         arr[i] = inFile.get();
    }
}

void Sort::Select(string type){
    if(type == "Merge"){
        funcPtr = Merge::sort;
    } else if(type == "Insertion"){
        funcPtr = Insertion::sort;
    } else{
        funcPtr = Bubble::sort;
    }
    this->sortType = type;
}

void Sort::Execute(bool disp, int arrSize, int origin, int dest){//origin and dest not used here, they are for search

    high_resolution_clock::time_point Start = high_resolution_clock::now();

    funcPtr(arr, 0, arrSize - 1, arrSize);

    high_resolution_clock::time_point End = high_resolution_clock::now();
    duration<double> clock  = duration_cast<duration<double>>(End - Start);

    sortTime = clock;

    if(disp == true){
        Display();
    }
}

void Sort::Display(){
    for (int i = 0; i < size; i++) {
        cout << "arr[" << i << "]: " << arr[i] << endl;
    }
}

void Sort::Stats(int& pathLength, int& pathCost, double& pathDistance, int& nodeNum, chrono::duration<double>& time){
    cout << sortType << " sort took " << sortTime.count() << " seconds" << endl;
    time = sortTime;
}

void Sort::Save(){
    ofstream outFile;

    if (dataType == "Random") {
        if(size == 10)
            outFile.open("random10(SOL).txt");
        else if(size == 1000)
            outFile.open("random1000(SOL).txt");
        else if(size == 10000)
            outFile.open("random10000(SOL).txt");
        else
            outFile.open("random100000(SOL).txt");
    }

    else if (dataType == "Reversed Sorted Order") {
        if(size == 10)
            outFile.open("revFile10(SOL).txt");
        else if(size == 1000)
            outFile.open("revFile1000(SOL).txt");
        else if(size == 10000)
            outFile.open("revFile10000(SOL).txt");
        else
            outFile.open("revFile100000(SOL).txt");
    }

    else if (dataType == "20 % unique values") {
        if(size == 10)
            outFile.open("unq10(SOL).txt");
        else if(size == 1000)
            outFile.open("unq1000(SOL).txt");
        else if(size == 10000)
            outFile.open("unq10000(SOL).txt");
        else
            outFile.open("unq100000(SOL).txt");
    }

    else if (dataType == "30 % randomized") {
        if(size == 10)
            outFile.open("partRand10(SOL).txt");
        else if(size == 1000)
            outFile.open("partRand1000(SOL).txt");
        else if(size == 10000)
            outFile.open("partRand10000(SOL).txt");
        else
            outFile.open("partRand100000(SOL).txt");
    }

    for(int i = 0; i < size; i++){
        outFile << arr[i] << ", ";
        //cout << arr[i] << ", ";
    }

    outFile.close();
}

Sort::~Sort(){
    delete [] arr;
}
