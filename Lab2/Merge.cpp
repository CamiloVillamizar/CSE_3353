#include "Merge.h"
#include <iostream>

using namespace std;

void Merge::sort(int* a, int low, int high, int size)//size not used in this sort method
{
	int mid;
	if (low < high)
	{
        mid = (low + high) / 2;
        sort(a, low, mid, size);//seperate the array into two
        sort(a, mid + 1, high, size);
        //mergeArrays(a, low, high, mid);//combine the two arrays again
        int i, j, k;
        int* temp = new int[high - low + 1];
        i = low;
        k = 0;
        j = mid + 1;

        while (i <= mid && j <= high)
        {
            if (a[i] < a[j])
            {
                temp[k] = a[i];
                k++;
                i++;
            }
            else
            {
                temp[k] = a[j];
                k++;
                j++;
            }
        }

        while (i <= mid)
        {
            temp[k] = a[i];
            k++;
            i++;
        }

        while (j <= high)
        {
            temp[k] = a[j];
            k++;
            j++;
        }

        for (i = low; i <= high; i++)
        {
            a[i] = temp[i - low];
        }
        delete [] temp;
	}
}
