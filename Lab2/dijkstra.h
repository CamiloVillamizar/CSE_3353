#ifndef DIJKSTRA_H
#define DIJKSTRA_H
#include <vector>
#include<queue>
#include <algorithm>
#include "path.h"
#include "position.h"
#include "location.h"

using namespace std;


class dijkstra
{
public:
    static vector<int> search(vector<vector<path>>, vector<position>, int, int, int&, int&);
};

#endif // DIJKSTRA_H
