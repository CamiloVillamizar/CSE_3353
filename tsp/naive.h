#ifndef NAIVE_H
#define NAIVE_H
#include <vector>
#include "position.h"
#include "path.h"

using namespace std;

class naive
{
public:
    naive();
    static vector<int> naiveSearch(vector<vector<path>>, vector<position>, double&);
    static void dumbSolution(vector<int>&, int, vector<position>, double&);
    static vector<vector<int>> getHamiltonians(vector<vector<int>>&);
    static double dist(position, position);
    static bool ifDone(int*, int);
};

#endif // NAIVE_H
