#ifndef POSITION_H
#define POSITION_H
#include <iostream>

using namespace std;

class position
{
private:
    double x;
    double y;
    double z;
public:
    position();
    position(double, double, double);
    void setX(double);
    void setY(double);
    void setZ(double);
    int getX();
    int getY();
    int getZ();

    friend ostream& operator<<(ostream& out, const position& pos){
        out << "(" << pos.x << ", " << pos.y << ", " << pos.z << ")";
        return out;
    }
};

#endif // POSITION_H
