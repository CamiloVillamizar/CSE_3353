#include "Sort.h"
#include "Insertion.h"
#include "Merge.h"
#include "Bubble.h"
#include <iostream>

using namespace std;

Sort::Sort(int* arr, int size, string type)
{
	if (type == "Insertion") {
		//cout << "Sorting by insertion method" << endl;
		Insertion sort(arr, size);
		sort.sort();
	}
	else if (type == "Bubble") {
		//cout << "Sorting by bubble method" << endl;
		Bubble sort(arr, size);
		sort.sort();
	}
	else if(type == "Merge"){
		//cout << "Sorting by merge method" << endl;
		Merge sort(arr, size);
		sort.sort(arr, 0, size - 1);
	}
}

Sort::~Sort()
{
}
