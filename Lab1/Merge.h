#pragma once
class Merge
{
private:
	int* arr, size;
public:
	Merge(int*, int);
	void sort(int*, int, int);
	void mergeArrays(int*, int, int, int);
	~Merge();
};

