#include "Insertion.h"
#include "Merge.h"
#include "Sort.h"
#include "Strategy.h"
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std::chrono;

Strategy::Strategy()
{
    this->size = 10;//set the default size to 10
    this->arr = nullptr;//initialize null values
    this->dataType = "";
}

void Strategy::load(int size, string dataType)
{
	this->size = size;
    this->arr = new int[size];
    this->dataType = dataType;

    ifstream inFile;//Creates a input stream object which will
    //be chosen below depending on what the current test is

	if (dataType == "Random") {
        if(size == 10)
            inFile.open("random10.txt", ifstream::in);
        else if(size == 1000)
            inFile.open("random1000.txt", ifstream::in);
        else if(size == 10000)
            inFile.open("random10000.txt", ifstream::in);
        else
            inFile.open("random100000.txt", ifstream::in);
	}

	else if (dataType == "Reversed Sorted Order") {
        if(size == 10)
            inFile.open("revFile10.txt", ifstream::in);
        else if(size == 1000)
            inFile.open("revFile1000.txt", ifstream::in);
        else if(size == 10000)
            inFile.open("revFile10000.txt", ifstream::in);
        else
            inFile.open("revFile100000.txt", ifstream::in);
	}

	else if (dataType == "20 % unique values") {
        if(size == 10)
            inFile.open("unq10.txt", ifstream::in);
        else if(size == 1000)
            inFile.open("unq1000.txt", ifstream::in);
        else if(size == 10000)
            inFile.open("unq10000.txt", ifstream::in);
        else
            inFile.open("unq100000.txt", ifstream::in);
	}

	else if (dataType == "30 % randomized") {
        if(size == 10)
            inFile.open("partRand10.txt", ifstream::in);
        else if(size == 1000)
            inFile.open("partRand1000.txt", ifstream::in);
        else if(size == 10000)
            inFile.open("partRand10000.txt", ifstream::in);
        else
            inFile.open("partRand100000.txt", ifstream::in);
	}

    for(int i = 0; i < size; i++){//loop pulls data in from the file into the array
         arr[i] = inFile.get();
    }

}

void Strategy::Execute(bool display)
{
    high_resolution_clock::time_point Start = high_resolution_clock::now();

    Sort sorter(arr, size, type);

    high_resolution_clock::time_point End = high_resolution_clock::now();
    duration<double> clock  = duration_cast<duration<double>>(End - Start);
    sortTime = clock;
    Stats();
    if(display == true)
        Display();
    Save();
}

void Strategy::Display()
{
	for (int i = 0; i < size; i++) {
		cout << "arr[" << i << "]: " << arr[i] << endl;
	}
}

void Strategy::Stats()
{
    cout << type << " sort took " << sortTime.count() << " seconds.\n" << endl;
}

void Strategy::Select(string input)
{
	type = input;
}

void Strategy::Save()
{
    ofstream outFile;

    if (dataType == "Random") {
        if(size == 10)
            outFile.open("random10(SOL).txt");
        else if(size == 1000)
            outFile.open("random1000(SOL).txt");
        else if(size == 10000)
            outFile.open("random10000(SOL).txt");
        else
            outFile.open("random100000(SOL).txt");
    }

    else if (dataType == "Reversed Sorted Order") {
        if(size == 10)
            outFile.open("revFile10(SOL).txt");
        else if(size == 1000)
            outFile.open("revFile1000(SOL).txt");
        else if(size == 10000)
            outFile.open("revFile10000(SOL).txt");
        else
            outFile.open("revFile100000(SOL).txt");
    }

    else if (dataType == "20 % unique values") {
        if(size == 10)
            outFile.open("unq10(SOL).txt");
        else if(size == 1000)
            outFile.open("unq1000(SOL).txt");
        else if(size == 10000)
            outFile.open("unq10000(SOL).txt");
        else
            outFile.open("unq100000(SOL).txt");
    }

    else if (dataType == "30 % randomized") {
        if(size == 10)
            outFile.open("partRand10(SOL).txt");
        else if(size == 1000)
            outFile.open("partRand1000(SOL).txt");
        else if(size == 10000)
            outFile.open("partRand10000(SOL).txt");
        else
            outFile.open("partRand100000(SOL).txt");
    }

    for(int i = 0; i < size; i++){
        outFile << arr[i] << ", ";
        //cout << arr[i] << ", ";
    }

    outFile.close();
}

void Strategy::configure()
{
    //for future labs
}

void Strategy::operator==(Strategy right)
{
	this->size = right.size;
	this->arr = right.arr;
	this->type = right.type;
}

Strategy::~Strategy()
{
    //delete arr;//this is suppoed to make sure memory doesn't leak,
    //but it makes my program crash and I can't figure out why :/
}
