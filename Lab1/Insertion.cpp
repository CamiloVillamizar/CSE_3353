#include "Insertion.h"

Insertion::Insertion(int* ptr, int sz)
{
	arr = ptr;
	size = sz;
}

void Insertion::sort()
{
	int temp, i, j;
	for(int i = 1; i < size; i++)
	{
		temp = arr[i];
		j = i - 1;
        while (j >= 0 && temp < arr[j])
		{
			arr[j + 1] = arr[j];
            j--;
		}
		arr[j + 1] = temp;
	}
}

Insertion::~Insertion()
{
}
