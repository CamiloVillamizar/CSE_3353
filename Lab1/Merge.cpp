#include "Merge.h"
#include <iostream>

using namespace std;

Merge::Merge(int* ptr, int sz)
{
	arr = ptr;
	size = sz;
}

void Merge::sort(int* a, int low, int high)
{
	int mid;
	if (low < high)
	{
        mid = (low + high) / 2;
        sort(a, low, mid);//seperate the array into two
		sort(a, mid + 1, high);
        mergeArrays(a, low, high, mid);//combine the two arrays again
	}
}

void Merge::mergeArrays(int *a, int low, int high, int mid)
{
	int i, j, k;
	int* temp = new int[high - low + 1];
	i = low;
	k = 0;
	j = mid + 1;

	while (i <= mid && j <= high)
	{
		if (a[i] < a[j])
		{
			temp[k] = a[i];
			k++;
			i++;
		}
		else
		{
			temp[k] = a[j];
			k++;
			j++;
		}
	}

	while (i <= mid)
	{
		temp[k] = a[i];
		k++;
		i++;
	}

	while (j <= high)
	{
		temp[k] = a[j];
		k++;
		j++;
	}

	for (i = low; i <= high; i++)
	{
		a[i] = temp[i - low];
	}
}

Merge::~Merge()
{
}
