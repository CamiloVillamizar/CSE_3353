#include "Bubble.h"
#include <string>//gives access to the swap function

Bubble::Bubble(int* ptr, int sz)
{
	arr = ptr;
	size = sz;
}

void Bubble::sort()
{
	int i, j;
	for (i = 0; i < size - 1; i++) {
		for (j = 0; j < size - i - 1; j++) {
			if (arr[j] > arr[j + 1]) {
                std::swap(arr[j], arr[j + 1]);
			}
		}
	}
}

Bubble::~Bubble()
{
}
