#include <iostream>
#include "Strategy.h"
#include <vector>
#include <fstream>

void generateFiles();
void randFile(int);
void revFile(int);
void unqFile(int);
void rand30File(int);

using namespace std;

int main()
{
    //generateFiles();//uncomment to generate new data files

    Strategy algo;//create algo object

    //int dataSize[] = {10};//Test Arrays
    //string dataTypes[] = {"Random"};
    //string sortTypes[] = {"Merge"};

    int dataSize[] = {10, 1000, 10000, 100000};//Real arrays
    string dataTypes[] = {"Random", "Reversed Sorted Order", "20 % unique values", "30 % randomized"};
    string sortTypes[] = {"Merge", "Insertion", "Bubble"};

    for(string currentMethod : sortTypes) {//Iterate through algos
        algo.Select(currentMethod);
        cout << "Begining " << currentMethod << " sort analysis" << endl;

        for (int currentSize : dataSize) {//Iterate through array sizes
            for (string currentType : dataTypes) {//Iterate through
                cout << currentMethod << ": " << currentSize << " -> " << currentType << endl;//let the user know
                //which combination of data and algos is being worked on

                algo.load(currentSize, currentType);//load file & prepare for sorting

                bool displayResultsToScreen = false;//set true if you want to see sorted arrays on screen(not recommended for the 1,000 and larger arrays)
                algo.Execute(displayResultsToScreen);//run algos

            }
        }
        cout << "===========================================================================" << endl;//helps identify when
        //an algo is done
    }
    cout << "End Program" << endl;//Makes it easier to see if the whole program executed
    return 0;
}

void generateFiles(){

    int sizes[] = {10, 1000, 10000, 100000};

    for(int size : sizes){
        randFile(size);
        revFile(size);
        unqFile(size);
        rand30File(size);
    }
    cout << "Done Creating files" << endl;
}

void randFile(int size){
    ofstream randomFile;
    if(size == 10)
        randomFile.open("random10.txt");
    else if(size == 1000)
        randomFile.open("random1000.txt");
    else if(size == 10000)
        randomFile.open("random10000.txt");
    else
        randomFile.open("random100000.txt");

    srand(time(NULL));
    for (int i = 0; i < size; i++) {
        randomFile << rand() % size + 1 << endl;
    }
    randomFile.close();
}

void revFile(int size){
    ofstream rev;

    if(size == 10)
        rev.open("revFile10.txt");
    else if(size == 1000)
        rev.open("revFile1000.txt");
    else if(size == 10000)
        rev.open("revFile10000.txt");
    else
        rev.open("revFile100000.txt");


    for (int i = 1; i <= size; i++) {
        rev << size + 1 - i << endl;
    }
    rev.close();
}

void unqFile(int size){
    int arr[] = {1, 2, 3, 4, 5};
    for(int i = 0; i < 25; i++){//makes an array of nums 1->5 in random order
        swap(arr[rand() % 5], arr[rand() % 5]);
    }

    ofstream unq;

    if(size == 10)
        unq.open("unq10.txt");
    else if(size == 1000)
        unq.open("unq1000.txt");
    else if(size == 10000)
        unq.open("unq10000.txt");
    else
        unq.open("unq100000.txt");


    for(int i = 0; i < size; i++){
        if (i < size * .2) {
            unq << arr[0] << endl;
        }
        else if (i < size * .4) {
            unq << arr[1] << endl;
        }
        else if (i < size * .6) {
            unq << arr[2] << endl;
        }
        else if (i < size * .8) {
            unq << arr[3] << endl;
        }
        else {
            unq << arr[4] << endl;
        }
    }
    unq.close();
}

void rand30File(int size){

    ofstream rand30;

    if(size == 10)
        rand30.open("partRand10.txt");
    else if(size == 1000)
        rand30.open("partRand1000.txt");
    else if(size == 10000)
        rand30.open("partRand10000.txt");
    else
        rand30.open("partRand100000.txt");


    int* arr = new int[size];
    for (int i = 0; i < size; i++) {
        arr[i] = i;
    }
    for (int i = 0; i < size * .3; i++) {
        swap(arr[rand() % size], arr[rand() % size]);
    }
    for (int i = 0; i < size; i++){
        rand30 << arr[i] << endl;
    }

    rand30.close();
    delete arr;
    arr = nullptr;
}


