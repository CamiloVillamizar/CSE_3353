#pragma once
#include "Strategy.h"

using namespace std;

class Sort : public Strategy
{
public:
	Sort(int*, int, string);
	~Sort();
};

