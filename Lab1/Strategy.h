#pragma once
#include <string>
#include <time.h>
#include <ctime>//ctime, ratio, and chrono give access to the clock
#include <ratio>
#include <chrono>

using namespace std;

class Strategy
{

protected:
	int size;
    int* arr;//int array which will be sorted
    chrono::duration<double> sortTime;//used to save info from execute to save function
    string type;//type of sort being done (Merge, Insertion, or Bubble)
    string dataType;//type of data being loaded (Random, Reversed... etc)
public:
	Strategy();
	void load(int, string);
    void Execute(bool);
	void Display();
	void Stats();
	void Select(string);
    void Save();
	void configure();
	void operator == (Strategy);
	~Strategy();
};

