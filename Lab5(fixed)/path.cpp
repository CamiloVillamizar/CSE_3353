#include "path.h"

path::path()
{
    dest = 0;
    cost = 0;
    visited = false;
}

path::path(int d){
    dest = d;
    cost = 0;
    visited = false;
}

path::path(int d, float c){
    dest = d;
    cost = c;
    visited = false;
}

void path::setDest(int val){
    dest = val;
}

void path::setCost(float val){
    cost = val;
}

void path::setVis(bool val){
    visited = val;
}

int path::getDest() const{
    return dest;
}

float path::getCost() const{
    return cost;
}

bool path::getVis() const{
    return visited;
}

const bool path::operator>(const path & arg) const
{
    if(cost > arg.cost)
        return true;
    return false;
}

const bool path::operator<(const path & arg) const
{
    if(cost < arg.cost)
        return true;
    return false;
}

const bool path::operator==(const path & arg) const
{
    if(cost == arg.cost)
        return true;
    return false;
}

path& path::operator= (const path& arg)
{
    dest = arg.dest;
    cost = arg.cost;
    visited = arg.visited;
}
