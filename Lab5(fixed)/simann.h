#ifndef SIMANN_H
#define SIMANN_H
#include "position.h"
#include <vector>
#include <math.h>

using namespace std;

class SimAnn
{
public:
    SimAnn();
    static vector<int> SimAnnSearch(double, vector<position>, double&);
    static double cost(vector<int>, vector<position>);
    static double dist(position, position);
    static vector<int> makeNeighbor(vector<int>, int);
    static double ProbCalc(double, double, double);
};

#endif // SIMANN_H
