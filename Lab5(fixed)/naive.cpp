#include "naive.h"
#include <math.h>
#include <iomanip>
#include <bits/stdc++.h>//import for int_max

naive::naive()
{

}

vector<int> naive::naiveSearch(vector<vector<path>> graph, vector<position>positions, double& pathCost){
    int* iterators = new int[graph.size()];

    for(unsigned int i = 0; i < graph.size(); i++){
        if(i != 0)
            iterators[i] = i;
        else
            iterators[i] = i + 1;
    }

    //double pathCost = TSP(matrix, finalPath, graph.size());//call naive search
    vector<int> finalPath;
    dumbSolution(finalPath, graph.size(), positions, pathCost);//call naive search

    //cout << "Path Cost: " << pathCost << endl;

    delete iterators;

    return finalPath;

}

double naive::dist(position p1, position p2){
    return sqrt(pow((p1.getX()-p2.getX()), 2)+pow((p1.getY()-p2.getY()), 2)+pow((p1.getZ()-p2.getZ()), 2));
}

bool naive::ifDone(int* arr, int size){
    for(int i = 0; i < size; i++){
        if(arr[i] != size){
            return false;
        }
    }
    return true;
}

void naive::dumbSolution(vector<int>& path, int graphSize, vector<position> positions, double& cost){
    vector<vector<int>> allPaths;
    cost = 0.0;
    int nodes[graphSize];
    for(int i = 0; i < graphSize; i++){
        nodes[i] = 1;
    }

    int* arr = new int[graphSize];
    arr = nodes;

    while(!ifDone(arr, graphSize)){

        vector<int> temp;

        for(int i = 0; i < graphSize; i++){
            temp.push_back(nodes[i]);
        }
        allPaths.push_back(temp);

        bool changed = false;

        while (!changed){//reset iterators to the right of the one that reached the "max" value
            for(int i = graphSize-1; i >= 0; i--){
                if(nodes[i] != graphSize){
                    nodes[i] += 1;
                    changed = true;
                    break;
                } else {
                    int sub = 1;
                    while(i-sub >= 0){
                        if(nodes[i-sub] != graphSize){
                            break;
                        }
                        sub++;
                    }
                    nodes[i-sub]++;
                    sub--;
                    while(sub >= 0){
                        nodes[i-sub] = 1;
                        sub--;
                        changed = true;
                    }
                }
                if (changed){
                    break;
                }
            }
        }

        //delete[] arr;
        arr = new int[graphSize];
        arr = nodes;
    }

    vector<int> temp;

    for(int i = 0; i < graphSize; i++){//one extra one for the final case where all itr
        temp.push_back(nodes[i]);
    }
    allPaths.push_back(temp);

    for(unsigned int i = 0; i < allPaths.size(); i++){//add the final node to each path
        allPaths[i].push_back(1);
    }

//    cout << "All paths: " << endl;//print all paths created
//    for(unsigned int i = 0; i < allPaths.size(); i++){
//        vector<int> temp = allPaths[i];
//        for(unsigned int j = 0; j < temp.size(); j++){
//            cout << temp[j] << ", ";
//        }
//        cout << endl;
//    }

    allPaths = getHamiltonians(allPaths);//strip path list of non-hamiltonian circuit

//    cout << "hamiltonian paths: " << endl;//Print all hamiltonian paths
//    for(unsigned int i = 0; i < allPaths.size(); i++){
//        vector<int> temp = allPaths[i];
//        for(unsigned int j = 0; j < temp.size(); j++){
//            cout << temp[j] << ", ";
//        }
//        cout << endl;
//    }

    vector<int> optimalPath;
    double optimalCost = double(INT_MAX);//set optimal cost to max int to ensure first path will overwrite it

    for(unsigned int i = 0; i < allPaths.size(); i++){
        vector<int> currentPath = allPaths[i];

        double currentPathCost = 0.0;

        for(unsigned int j = 0; j < currentPath.size()-1; j++){
            currentPathCost += dist(positions[currentPath[j] - 1], positions[currentPath[j+1] - 1]);
        }

        if(optimalCost > currentPathCost){
            optimalCost = currentPathCost;
            optimalPath = currentPath;
        }
    }

    cost = optimalCost;//set cost var passed by reference to the optimal cost so it can be "returned" by reference
    path = optimalPath;

}

vector<vector<int>> naive::getHamiltonians(vector<vector<int>>& list){
    vector<vector<int>> hPaths;

    bool check[list[0].size()];

    for(unsigned int i = 0; i < list.size(); i++){
        for(unsigned int j = 0; j < list[0].size(); j++){
            check[j] = false;
        }
        vector<int> temp = list[i];

        if(temp[0] == 1){

            for(unsigned int j = 0; j < temp.size(); j++){
                check[temp[j]-1] = true;
            }

            bool addToList = true;
            for(unsigned int j = 0; j < list[0].size() - 1; j++){
                if(check[j] == false){
                    addToList = false;
                    break;
                }
            }

            if(addToList == true){
                hPaths.push_back(temp);
            }
        }
    }
    return hPaths;
}
