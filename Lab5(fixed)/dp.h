#ifndef DP_H
#define DP_H
#include <vector>
#include "position.h"
#include "path.h"

class DP
{
public:
    DP();
    static vector<int> DPSearch(vector<vector<path>>, vector<position>, double&);
    static double dist(position, position);
    static bool ifDone(bool*, int);
};

#endif // DP_H
