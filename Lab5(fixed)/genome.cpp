#include "genome.h"

genome::genome()
{

}

genome::genome(vector<int> path){
    this->path = path;
    this->fitness = 0;
}

genome::genome(vector<int> path, double fitness){
    this->path = path;
    this->fitness = fitness;
}

void genome::setPath(vector<int> newPath){
    this->path = newPath;
}

vector<int> genome::getPath(){
    return path;
}

void genome::setFitness(double newFitness){
    this->fitness = newFitness;
}

double genome::getFitness(){
    return fitness;
}

const bool genome::operator==(const genome & right) const{
    return this->fitness == right.fitness;
}

const bool genome::operator<(const genome & right) const{
    return this->fitness < right.fitness;
}

const bool genome::operator>(const genome & right) const{
    return this->fitness > right.fitness;
}
