#include "dp.h"
#include <math.h>
#include <bits/stdc++.h>//import for int_max
#include <iomanip>

DP::DP()
{

}

vector<int> DP::DPSearch(vector<vector<path>> graph, vector<position> positions, double& cost){
    vector<int> finalPath;//initialize final path vector
    bool* visited = new bool[graph.size()];//array for keeping track of visited nodes

    int currentNode = 1;//create and initialize values for the DP search
    int nextNode = NULL;
    double nextDist = double(INT_MAX);
    visited[currentNode - 1] = true;
    cost = 0;
    finalPath.push_back(currentNode);

    while(!ifDone(visited, graph.size())){//while not done
        for(unsigned int i = 0; i < graph.size(); i++){//
            if(visited[i] == false){//for each unvisited node
                double temp = dist(positions[currentNode - 1], positions[i]);
                if(dist(positions[currentNode - 1], positions[i]) < nextDist){//find the next closest node
                    nextNode = i + 1;
                    //nextDist = matrix[currentNode - 1][i];
                    nextDist = dist(positions[currentNode - 1], positions[i]);
                }
            }
        }
        currentNode = nextNode;//move to next node and add the cost to the total
        finalPath.push_back(currentNode);
        cost += nextDist;

        visited[currentNode - 1] = true;//mark current node as visited
        nextDist = double(INT_MAX);//reset next distance to max for the next override
    }


    currentNode = nextNode;//add the return to the starting node to the path and cost
    nextNode = 1;
    finalPath.push_back(1);
    nextDist = dist(positions[currentNode - 1], positions[nextNode - 1]);
    cost += nextDist;

    return finalPath;
}

double DP::dist(position p1, position p2){
    return sqrt(pow((p1.getX()-p2.getX()), 2)+pow((p1.getY()-p2.getY()), 2)+pow((p1.getZ()-p2.getZ()), 2));//3d distance formula
}

bool DP::ifDone(bool* arr, int size){
    for(int i = 0; i < size; i++){//check if all values in the array are true
        if(arr[i] != true){
            return false;
        }
    }
    return true;
}
