#include "pso.h"

PSO::PSO()
{

}

vector<int> PSO::PSOSearch(double goalCost, vector<position> positions, double& cost){

    int populationSize = 50;
    int pathLength = positions.size() + 1;
    vector<particle> population;

    if(populationSize % 2 == 1){
        populationSize--;//population initialization same as GA
    }

    for(int i = 0; i < populationSize; i++){//Create population
        vector<int> temp;
        for(int j = 0; j < pathLength; j++){
            if(j == pathLength - 1){
                temp.push_back(1);
            } else {
                temp.push_back(j + 1);
            }
        }
        particle tempparticle(temp);
        population.push_back(tempparticle);
        temp.clear();
    }/////////////////////////////////////////////////////////////


    srand(time(NULL));//randomize rand() seed with the time value

    for(int i = 0; i < populationSize; i++){//Randomize the initial population
        for(int j = 0; j < pathLength/2; j++){
            int rand1 = ((int)rand() % (pathLength - 2))  + 1;
            int rand2 = ((int)rand() % (pathLength - 2)) + 1;

            while(rand1 == rand2){
                rand2 = ((int)rand() % (pathLength - 2))  + 1;
            }

            vector<int> tempPath = population[i].getPath();
            swap(tempPath[rand1], tempPath[rand2]);
            population[i].setPath(tempPath);
        }
    }/////////////////////////////////////////////////////////////////////////////////////////

    for(int i = 0; i < populationSize; i++){//Initialize fitnesses
        double currentFitness = 0.0;
        for(int j = 0; j < pathLength - 1; j++){
            currentFitness += dist(positions[population[i].getPath()[j] - 1], positions[population[i].getPath()[j+1] - 1]);
        }
        population[i].setFitness(currentFitness);
    }/////////////////////////////////////////////////////////////////

    std::sort(population.begin(), population.end());

    int pBest = INT_MAX;
    int gBest = INT_MAX;
    while(true){//infinite while loop because at the end there is a check that will break the loop if it should

        int topIndex = 0;
        while(population[topIndex].getFitness() == 0){//get top fitness (EXIT CONDITION 1)
            topIndex++;
            if(topIndex == population.size()){
                topIndex = 0;
                break;
            }
        }

        if (population[topIndex].getFitness() < pBest){
            pBest = topIndex;
        }

        if (population[pBest].getFitness() < gBest){
            gBest = pBest;
        }

        //update each particle based on these two equations

        //v[] = v[] + c1 * rand() * (pbest[] - present[]) + c2 * rand() * (gbest[] - present[])//update the velocity to
        //newVelocity = prevVelocity + learningFactor1 * (rand number [0, 1]) * (pBestVel - currentParticleSol) + LearningFactor2 + (rand number [0, 1]) * (gBestVel - currentParticleSol)
        //present[] = present[] + v[] (b)
        //currentSolution = currentSolution + velocity

    }

}

double PSO::dist(position p1, position p2){
    return sqrt(pow((p1.getX()-p2.getX()), 2)+pow((p1.getY()-p2.getY()), 2)+pow((p1.getZ()-p2.getZ()), 2));
}
