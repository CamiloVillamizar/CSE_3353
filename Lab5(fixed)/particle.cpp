#include "particle.h"

particle::particle()
{

}

particle::particle(vector<int> path){
    this->path = path;
    this->fitness = 0;
}

void particle::setPath(vector<int> newPath){
    this->path = newPath;
}

vector<int> particle::getPath(){
    return path;
}

void particle::setFitness(double newFitness){
    this->fitness = newFitness;
}

double particle::getFitness(){
    return fitness;
}

const bool particle::operator==(const particle & right) const{
    return this->fitness == right.fitness;
}

const bool particle::operator<(const particle & right) const{
    return this->fitness < right.fitness;
}

const bool particle::operator>(const particle & right) const{
    return this->fitness > right.fitness;
}
