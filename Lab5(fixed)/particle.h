#ifndef PRTICLE_H
#define PRTICLE_H
#include <vector>
using namespace std;

class particle
{
private:
    vector<int> path;
    double fitness;
    double veocity;
public:
    particle();
    particle(vector<int>);
    void setPath(vector<int>);
    void setFitness(double);
    vector<int> getPath();
    double getFitness();
    const bool operator==(const particle&) const;
    const bool operator>(const particle&) const;
    const bool operator<(const particle&) const;
};

#endif // PRTICLE_H
