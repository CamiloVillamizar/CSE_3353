#include "simann.h"

SimAnn::SimAnn()
{

}

vector<int> SimAnnSearch(double goalCost, vector<position> positions, double& cost){
//    int pathLength = positions.size() + 1;
//    vector<int> bestPath;
//    for(int j = 0; j < pathLength; j++){//generate initial solution
//        if(j == pathLength - 1){
//            bestPath.push_back(1);
//        } else {
//            bestPath.push_back(j + 1);
//        }
//    }

//    double oldCost = cost(bestPath, positions);
//    double temperature = 1.0;
//    double TempMin = 0.00001;
//    double tempChangeVal = 0.98;


//    while (temperature > TempMin){
//        int iterations = 1;

//        while (iterations < 100){
//            vector<int> newPath = makeNeighbor(bestPath, pathLength);
//            double newCost = cost(newPath, positions);

//            double prob = 0.5;//acceptance Probability calculation(old cost, new cost, current temperature)

//            if (prob > (rand() % 1 + 1)){
//                    bestPath = newPath;
//                    oldCost = newCost;
//            }
//            iterations++;
//        }
//        temperature = temperature * tempChangeVal;
//    }

//    return bestPath;

}

double SimAnn::cost(vector<int> path, vector<position> positions){
    double currentFitness = 0;
    for(int j = 0; j < path.size() - 1; j++){
        currentFitness += dist(positions[path[j] - 1], positions[path[j+1] - 1]);
    }
    return currentFitness;
}

double SimAnn::dist(position p1, position p2){
    return sqrt(pow((p1.getX()-p2.getX()), 2)+pow((p1.getY()-p2.getY()), 2)+pow((p1.getZ()-p2.getZ()), 2));
}

vector<int> SimAnn::makeNeighbor(vector<int>& path, int pathLength){
    int rand1 = ((int)rand() % (pathLength - 2))  + 1;//take two random cities and swap them
    int rand2 = ((int)rand() % (pathLength - 2))  + 1;

    while(rand1 == rand2){//make sure its not the same city
        rand2 = ((int)rand() % (pathLength - 2))  + 1;
    }

    swap(path[rand1], path[rand2]);

}
