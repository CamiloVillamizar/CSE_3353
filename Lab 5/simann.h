#ifndef SIMANN_H
#define SIMANN_H
#include "position.h"
#include <vector>
#include <math.h>
class SimAnn
{
public:
    SimAnn();
    static vector<int> SimAnnSearch(double, vector<position>, double&);
    static double cost(vector<int>, vector<position>);
    static double dist(position, position);
    static vector<int> makeNeighbor(vector<int>&, int);
};

#endif // SIMANN_H
