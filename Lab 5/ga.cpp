#include "ga.h"

GA::GA()
{

}

vector<int> GA::GASearch(double goalCost, vector<position> positions, double& cost){

    int selectionMethod = 2;//1, 2, or 3 based on what type of selection you want

    int counter = 1;
    cost = 0;//reset cost value so that if it comes in with a preset value, it is overrided to 0
    int pathLength = positions.size() + 1;
    vector<int> finalPath;
    int populationSize = 50;
//    int populationSize = positions.size() - (positions.size()%4);
    vector<genome> population;

    //double distanceMatrix[pathLength][pathLength];

    if(populationSize % 2 == 1){
        populationSize--;//bc for some reason pop sizes of 9 and 13 break everything so make them an even number and it works idk why -\(~.~)/-
    }

    for(int i = 0; i < populationSize; i++){//CREATE BASIC POPULATION (i.e 1, 2, 3, 4, 5.. 12, 1)
        vector<int> temp;
        for(int j = 0; j < pathLength; j++){
            if(j == pathLength - 1){
                temp.push_back(1);
            } else {
                temp.push_back(j + 1);
            }
        }
        genome tempGenome(temp);
        population.push_back(tempGenome);
        temp.clear();
    }/////////////////////////////////////////////////////////////


    srand(time(NULL));

    for(int i = 0; i < populationSize; i++){//RANDOM POPULATION INITIALIZATION
        for(int j = 0; j < pathLength/2; j++){
            int rand1 = ((int)rand() % (pathLength - 2))  + 1;
            int rand2 = ((int)rand() % (pathLength - 2)) + 1;

            while(rand1 == rand2){
                rand2 = ((int)rand() % (pathLength - 2))  + 1;
            }

            vector<int> tempPath = population[i].getPath();
            swap(tempPath[rand1], tempPath[rand2]);
            population[i].setPath(tempPath);
        }
    }/////////////////////////////////////////////////////////////////////////////////////////

//        cout << "Population" << endl;
//        for(int i = 1; i < populationSize; i++){//DISPLAY POPULATION (skips the first one so the "straight path" is tested too
//            cout << i + 1 << " -> ";
//            for(int j = 0; j < pathLength; j++){
//                if(j == pathLength - 1){
//                    cout << population[i].getPath()[j];
//                }else {
//                    cout << population[i].getPath()[j] << ", ";
//                }
//            }
//            cout << endl;
//        }///////////////////////////////////////////////////////////

    bool continueWhile = true;
    bool firstIteration = true;
    while(continueWhile){
        unsigned int topIndex = 0;


        while(population[topIndex].getFitness() == 0){//get top fitness (EXIT CONDITION 1)
            topIndex++;
            if(topIndex == population.size()){
                topIndex = 0;
                break;
            }
        }

        if(firstIteration == false){//check if top fitness is equal to or better than the solution given by Tabu search
            //if it is, break out of the loop
            if(population[topIndex].getFitness() <= goalCost){
                continueWhile = false;//primiary exit solution -> if the best fitness is == to the best solution found by tabu (of which the fitness was passed in)
            }
        } else {
            firstIteration = false;
        }//////////////////////////////////////////////////////////////

        if(counter > 10000){//backup exit condition if the GA is having trouble finding the solution so it doesnt go on forever(EXIT CONDITION 2)
            break;
        } else {
            counter++;
        }

        //        float topFitness = population[0].getFitness();
        //        for(int i = 0; i <= population.size()/2; i++){
        //            cout << "i: " << i << "; " << population[i].getFitness() << endl;
        //            if(population[i].getFitness() != topFitness){
        //                cout << "Breaking" << endl;
        //                break;
        //            }
        //            if(i == population.size()/2){
        //                continueWhile = false;
        //            }
        //        }

        for(int i = 0; i < populationSize; i++){//INITIALIZE FITNESS
            double currentFitness = 0.0;
            for(int j = 0; j < pathLength - 1; j++){
                currentFitness += dist(positions[population[i].getPath()[j] - 1], positions[population[i].getPath()[j+1] - 1]);
            }
            population[i].setFitness(currentFitness);
        }/////////////////////////////////////////////////////////////////

        //        for(int i = 0; i < populationSize; i++){//INITIALIZE FITNESS
        //            double currentFitness = 0.0;
        //            for(unsigned int j = 0; j < pathLength - 1; j++){
        //                genome tempGenome = population[i];
        //                vector<int> tempPath = tempGenome.getPath();

        //                currentFitness += dist(positions[tempPath[j] - 1], positions[tempPath[j+1] - 1]);
        //            }
        //            population[i].setFitness(currentFitness);
        //        }/////////////////////////////////////////////////////////////////


        std::sort(population.begin(), population.end());//sort all the genomes to get the "most fit" ones at the top of the list

        srand(time(NULL));
        vector<genome> survivors = selection(population, selectionMethod);//SURVIVOR SELECTION (1, 2, or 3 for different selection methods)

        population = survivors;//reset population to the survivors

        for(unsigned int i = 0; i < survivors.size(); i += 2){//CROSSOVER
            vector<int> temp1 = survivors[i].getPath();
            vector<int> temp2 = survivors[i + 1].getPath();

            temp1 = crossover(temp1, temp2, pathLength, 1);


            //////////////////////Mutation (creates two "children" based on the combination of the two genomes which was just created)

            vector<int> child1 = temp1;
            vector<int> child2 = temp1;

            mutation(child1, pathLength, 1);//mutate childrens 1 and 2 and send them into the population
            mutation(child2, pathLength, 1);

            population.push_back(child1);
            population.push_back(child2);//////////////////////////////rebuild population back to the original size with th
        }//////////////////////////////////////////////////////////
    }

    //    cout << "Fitness size: " << population.size( << endl;
    //    cout << "Top fitnesses" << endl;
    //    for(int i = 0; i < population.size(); i++){
    //        cout << population[i].getFitness() << endl;
    //    }

    int topIndex = 0;

    while(population[topIndex].getFitness() == 0){
        topIndex++;
    }

    finalPath = population[topIndex].getPath();
    cost = population[topIndex].getFitness();
    return finalPath;
}

void GA::mutation(vector<int>& child, int pathLength, int method){
    if(method == 1){
        int rand1 = ((int)rand() % (pathLength - 2))  + 1;//take two random cities and swap them
        int rand2 = ((int)rand() % (pathLength - 2))  + 1;

        while(rand1 == rand2){//make sure its not the same city
            rand2 = ((int)rand() % (pathLength - 2))  + 1;
        }

        swap(child[rand1], child[rand2]);
    } else if(method == 2){
        vector<int> newChild;
        newChild.push_back(child[0]);//swap the first and second halves o

        for(unsigned int i = child.size()/2; i < child.size()-1; i++){
            newChild.push_back(child[i]);
        }

        for(unsigned int i = 1; i < child.size()/2; i++){
            newChild.push_back(child[i]);
        }
        newChild.push_back(child[child.size()-1]);
        child = newChild;
    }
}

vector<int> GA::crossover(vector<int> temp1, vector<int>temp2, int pathLength, int method){
    if(method == 1){
        if(temp2.size() == 0){
            temp2 = temp1;
        }

        int rand1 = ((int)rand() % (pathLength - 2))  + 1;
        int temp = temp1[rand1];
        int temp1Index = rand1;
        int temp2Index = 0;

        for(int j = 0; j < pathLength; j++){
            if(temp2[j] == temp){
                temp2Index = j;
                break;
            }
        }
        swap(temp1[temp1Index], temp1[temp2Index]);
        return temp1;
    } else if (method == 2){

    }
}

vector<genome> GA::selection(vector<genome> population, int method){//Survivor selection AKA THANOS SNAP
    vector<genome> survivors;

    if(method == 1){
        for(unsigned int i = 0; i < population.size()/2; i++){//top half elitism
            survivors.push_back(population[i]);
        }
    } else if(method == 2){//top quarter, then the rest are each other one (ex 1,2,3,4,5,6,7,8,9,10,11,12 -> 1, 2, 3, 4, 6, 8)
        int genomesToAdd = population.size()/2;
        int popIndex = 0;
        int genomesAdded = 0;
        for(int i = 0; i < genomesToAdd/2; i++){
            survivors.push_back(population[popIndex]);
            popIndex++;
            genomesAdded++;
        }
        while(genomesAdded < genomesToAdd){
            survivors.push_back(population[popIndex]);
            popIndex += 2;
            genomesAdded++;
        }
    } else {//every other genome
        for(unsigned int i = 0; i < population.size()/2; i += 2){
            survivors.push_back(population[i]);
        }
    }
    return survivors;
}


double GA::dist(position p1, position p2){
    return sqrt(pow((p1.getX()-p2.getX()), 2)+pow((p1.getY()-p2.getY()), 2)+pow((p1.getZ()-p2.getZ()), 2));
}
