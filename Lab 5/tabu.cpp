#include "tabu.h"

Tabu::Tabu()
{

}

vector<int> Tabu::TabuSearch(vector<vector<path>> graph, vector<position> positions, double& cost){

    int pathLength = positions.size() + 1;
    vector<int> finalPath;
    vector<vector<int>> neighbors;
    vector<double> fitness;
    vector<int> currentOptimalPath;

    for(int j = 0; j < pathLength; j++){
        if(j == pathLength - 1){
            currentOptimalPath.push_back(1);
        } else {
            currentOptimalPath.push_back(j + 1);
        }
    }

    vector<vector<int>> tabuList;//the 3 latest pairs of switches for the tabuList
    int tabuListSize = 3;
    bool leaveLoop = false;
    double curMin = INT64_MAX;

    while(!leaveLoop){
        leaveLoop = true;//loop is set to repeat by default

        neighbors = makeNeighborList(currentOptimalPath);//obtain neighbors list from method



//        for(unsigned int i = 0; i < neighbors.size(); i++){
//            for(unsigned int j = 0; j < neighbors[i].size(); j++){
//                cout << neighbors[i][j] << ", ";
//            }
//            cout << endl;
//        }

        double tempMin = curMin;
        vector<int> tempMinPath = currentOptimalPath;

        for(unsigned int i = 0; i < neighbors.size(); i++){
            vector<int> temp = neighbors[i];
            double currentFitness = 0;
            for(int j = 0; j < pathLength - 1; j++){
                currentFitness += dist(positions[temp[j] - 1], positions[temp[j+1] - 1]);
            }
            vector<int> indPair;
            for(int j = 0; j < currentOptimalPath.size(); j++){
                int x = currentOptimalPath[j];
                int y = neighbors[i][j];
                if(x != y){
                    //cout << "Adding: " << currentOptimalPath[j] << endl;
                    indPair.push_back(currentOptimalPath[j]);
                    if(indPair.size() == 2){
                        break;
                    }
                }
            }

//            cout << "Pair: " << endl;
//            for(int i : indPair){
//                cout << i << endl;
//                if(indPair.size() != 2){
//                    cout << "OP: ->";
//                    for(int i : currentOptimalPath){
//                        cout << i << ", ";
//                    }
//                    cout << endl;
//                    cout << "NB: ->";
//                    for(int i : neighbors[i]){
//                        cout << i << ", ";
//                    }
//                }
//            }

            if(!inTList(tabuList, indPair)){
                if (currentFitness < curMin) {
                    curMin = currentFitness;
                    currentOptimalPath = neighbors[i];
                    leaveLoop = false;
                    updateTabuList(tabuList, indPair, tabuListSize);
                }
            }
        }
    }

    cost = curMin;
    finalPath = currentOptimalPath;
    return finalPath;
}

void Tabu::updateTabuList(vector<vector<int>>& tabuList, vector<int> newTabu, int listSize){
    //cout << "TABU UPDATE--------------------" << endl;
    if(tabuList.size() >= listSize){
        tabuList.erase(tabuList.begin());
    }
    tabuList.push_back(newTabu);
}

bool Tabu::inTList(vector<vector<int>> tabuList, vector<int> pairToCheck){
    for(int i = 0; i < tabuList.size(); i++){
        if(tabuList[i] == pairToCheck){
            return true;
        }
    }
    return false;
}

vector<vector<int>> Tabu::makeNeighborList(vector<int> currentPath){
    vector<vector<int>> NList;


    for(unsigned int i = 1; i < currentPath.size()-1; i++){
        for(unsigned int j = i + 1; j < currentPath.size()-1; j++){
            //cout << i << ", " << j << endl;
            vector<int> temp = currentPath;
            swap(temp[i], temp[j]);
//            for(unsigned int k = 0; k < temp.size(); k++){
//                cout << temp[k] << ", ";
//            }
//            cout << endl;
            NList.push_back(temp);
        }
    }
    return NList;
}

double Tabu::dist(position p1, position p2){
    return sqrt(pow((p1.getX()-p2.getX()), 2)+pow((p1.getY()-p2.getY()), 2)+pow((p1.getZ()-p2.getZ()), 2));
}
