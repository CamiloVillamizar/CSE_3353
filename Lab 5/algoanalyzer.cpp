#include "algoanalyzer.h"

AlgoAnalyzer::AlgoAnalyzer()
{

}

void AlgoAnalyzer::run(){
    int numOfFiles = 5;
    files(numOfFiles);//Uncomment this and pass in a number to generate graph and position files with that many number
    cout << "Running program with " << numOfFiles << " cities.\n\n" << endl;
    ifstream inFile;
    inFile.open("graph.txt");//open graph file

    vector<vector<path>> adjList;//declare variables
    vector<vector<path>> shortList;
    vector<position> positions;

    string buffer;//string to take care of file input
    string strTemp;
    string strCheck;
    int commaNum = 0;
    int tempf;
    float curRow;//keeps track of the current row
    bool rowSet = false;

    cout << "Loading graph file..." << endl;

    while(!inFile.eof()){//loop sets up the tree for connections
        getline(inFile, buffer);
        commaNum = getCommaNum(buffer);
        //cout << commaNum << ": " << buffer << endl;

        if(buffer != ""){
            for(int i = 0; i < commaNum; i++){
                tempf = (int)stof(buffer.substr(0, buffer.find(',')));//convert text from file to intq

                if(rowSet == false){
                    curRow = tempf;
                    vector<path> newVect;
                    adjList.push_back(newVect);
                    rowSet = true;
                } else {
                    path newPath(tempf);
                    adjList[curRow - 1].push_back(newPath);
                }
                buffer = buffer.substr(buffer.find(',') + 1, buffer.length() - buffer.find(',') - 1);
            }
            tempf = (int)stof(buffer);
            adjList[curRow - 1].push_back(tempf);
            rowSet = false;
        }
    }

    inFile.close();
    cout << "Done" << endl;

    //printList(adjList);

    cout << "Loading positions..." << endl;
    getPositions(positions);
    cout << "Done\n\n" << endl;

    //(positions);


    vector<int> (*naiveStrategy)(vector<vector<path>>, vector<position>, double&);
    vector<int> (*DPStrategy)(vector<vector<path>>, vector<position>, double&);
    vector<int> (*GeneticAlgo)(double, vector<position>, double&);
    vector<int> (*Tabu)(vector<vector<path>>, vector<position>, double&);
    vector<int> (*PSOFunc)(double, vector<position>, double&);
    vector<int> (*SimAnnFunc)(double, vector<position>, double&);
    naiveStrategy = naive::naiveSearch;
    DPStrategy = DP::DPSearch;
    GeneticAlgo = GA::GASearch;
    Tabu = Tabu::TabuSearch;
    PSOFunc = PSO::PSOSearch;
    SimAnnFunc = SimAnn::SimAnnSearch;

    double naiveCost = 0.0;
    double dpCost = 0.0;
    double tabuCost = 0.0;
    double GACost = 0.0;


    high_resolution_clock::time_point t1 = high_resolution_clock::now();//first clock point
    vector<int> naivePath = naiveStrategy(adjList, positions, naiveCost);
    cout << "Naive Done" << endl;

    high_resolution_clock::time_point t2 = high_resolution_clock::now();//second clock point
    vector<int> DPPath = DPStrategy(adjList, positions, dpCost);
    cout << "DP Done" << endl;

    high_resolution_clock::time_point t3 = high_resolution_clock::now();//third clock point
    vector<int> TabuPath = Tabu(adjList, positions, tabuCost);
    cout << "Tabu done" << endl;

    high_resolution_clock::time_point t4 = high_resolution_clock::now();//fourth clock point
    vector<int> GAPath = GeneticAlgo(tabuCost, positions, GACost);
    cout << "GA Done" << endl;

    high_resolution_clock::time_point t5 = high_resolution_clock::now();//fifth clock point
    //vector<int> PSO= PSOFunc(tabuCost, positions, GACost);//methods are incomplete
    cout << "PSO Done" << endl;

    high_resolution_clock::time_point t6 = high_resolution_clock::now();//sixth clock point
    //vector<int> SimAnn = SimAnnFunc(tabuCost, positions, GACost);
    cout << "PSO Done" << endl;

    high_resolution_clock::time_point t7 = high_resolution_clock::now();//seventh clock point


    duration<double> naiveTime = duration_cast<duration<double>>(t2 - t1);
    duration<double> dpTime = duration_cast<duration<double>>(t3 - t2);
    duration<double> tabuTime = duration_cast<duration<double>>(t4 - t3);
    duration<double> GATime = duration_cast<duration<double>>(t5 - t4);

    cout << "Naive Brute Force -> " << endl;//display stats for naive search
    displayPath(naivePath);
    cout << "Path length: " << naiveCost << endl;
    cout << "Calculation Time: " << naiveTime.count() << endl;
    cout << endl;

    cout << "Dynamic Programming ->" << endl;//display stats for dynamic programming
    displayPath(DPPath);
    cout << "Path length: " << dpCost << endl;
    cout << "Calculation Time: " << dpTime.count() << endl;
    cout << endl;

    cout << "Tabu Search -> " << endl;//display stats for naive search
    displayPath(TabuPath);
    cout << "Path length: " << naiveCost << endl;
    cout << "Calculation Time: " << tabuTime.count() << endl;
    cout << endl;

    cout << "Genetic Algorithm ->" << endl;//display stats for dynamic programming
    displayPath(GAPath);
    cout << "Path length: " << dpCost << endl;
    cout << "Calculation Time: " << GATime.count() << endl;
    cout << endl;

//    cout << "Particle Swarm ->" << endl;//display stats for dynamic programming
//    displayPath(PSO);
//    cout << "Path length: " << dpCost << endl;
//    cout << "Calculation Time: " << GATime.count() << endl;
//    cout << endl;

//    cout << "Simulated Annealing ->" << endl;//display stats for dynamic programming
//    displayPath(SimAnn);
//    cout << "Path length: " << dpCost << endl;
//    cout << "Calculation Time: " << GATime.count() << endl;
//    cout << endl;


    cout << "End Program" << endl;
}

int AlgoAnalyzer::getCommaNum(string input){//returns how many commas are in the string passed in
    int commaCount = 0;
    for(char c : input){
        if(c == ','){
            commaCount++;
        }
    }
    return commaCount;
}

void AlgoAnalyzer::printList(vector<vector<path>> list){
    cout << "PRINT LIST===========" << endl;
    for(unsigned int i = 0; i < list.size(); i++){
        cout << i + 1 << " -> ";
        for(unsigned int j = 0; j < list[i].size(); j++) {
            if (j != list[i].size() - 1)
                cout << list[i][j].getDest() << ", ";
            else {
                cout << list[i][j].getDest();
            }
        }
        cout << endl;
    }
}

void AlgoAnalyzer::getPositions(vector<position>& positions){
    ifstream inFile;
    inFile.open("positions.txt");

    if(!inFile.is_open()){
        cout << "Positions didn't open" << endl;
    }

    string buffer;
    int commaNum = 0;
    int currentNum = 1;
    double x = 0;
    double y = 0;
    double z = 0;
    double tempVal;

    while(!inFile.eof()){
        getline(inFile, buffer);
        commaNum = getCommaNum(buffer);

        if(buffer != ""){
            for(int i = 0; i < commaNum; i++){
                tempVal = stof(buffer.substr(0, buffer.find(',')));

                if(currentNum == 1){
                    currentNum++;
                } else if(currentNum == 2){
                    x = tempVal;
                    currentNum++;
                } else if(currentNum == 3){
                    y = tempVal;
                    currentNum++;
                } else {
                    z = tempVal;
                    currentNum = 1;
                }
                buffer = buffer.substr(buffer.find(',') + 1, buffer.length() - buffer.find(',') - 1);
            }

            tempVal = stof(buffer);
            if(currentNum == 1){
                currentNum++;
            } else if(currentNum == 2){
                x = tempVal;
                currentNum++;
            } else if(currentNum == 3){
                y = tempVal;
                currentNum++;
            } else {
                z = tempVal;
                currentNum = 1;
            }

            position curPos(x, y, z);
            positions.push_back(curPos);
        }
    }
}

void AlgoAnalyzer::printPositions(vector<position> positions){
    cout << "Positions" << endl;
    for(unsigned int i = 0; i < positions.size(); i++){
        cout << i + 1 << " -> " << positions[i] << endl;
    }
}

void AlgoAnalyzer::displayPath(vector<int> finalPath){
    cout << "FinalPath ->" << endl;
    for(unsigned int i = 0; i < finalPath.size(); i++){
        if(i != finalPath.size() - 1)
            cout << finalPath[i] << ", ";
        else
            cout << finalPath[i] << endl;
    }
}

void AlgoAnalyzer::files(int numNodes){
   ofstream gFile;
   gFile.open("graph.txt");

   for(int i = 1; i <= numNodes; i++){
       gFile << i << ",";

       for(int j = 1; j <= numNodes; j++){
           if(j != i){
               if(i == numNodes){
                   if(j == numNodes - 1){
                       gFile << j;
                   } else {
                       gFile << j << ",";
                   }
               }else{
                   if(j == numNodes){
                       gFile << j;
                   } else {
                       gFile << j << ",";
                   }

               }
           }
       }
       gFile << endl;

   }

    ofstream pFile;
    pFile.open("positions.txt");

    srand((unsigned)time(0));

    for(int i = 0; i < numNodes; i++){
        pFile << i + 1 << "," << double(rand()%500)/100 << "," << double(rand()%500)/100 << "," << double(rand()%500)/100 << endl;
    }



    pFile.close();
}
