#ifndef PSO_H
#define PSO_H
#include <vector>
#include <climits>
#include <math.h>
#include <algorithm>
#include "particle.h"
#include "position.h"

using namespace std;

class PSO
{
public:
    PSO();
    static vector<int> PSOSearch(double, vector<position>, double&);
    static double dist(position, position);
};

#endif // PSO_H
