#include "position.h"

position::position()
{

}

position::position(double x, double y, double z){
    this->x = x;
    this->y = y;
    this->z = z;
}

void position::setX(double x){
    this->x = x;
}

void position::setY(double y){
    this->y = y;
}

void position::setZ(double z){
    this->z = z;
}

int position::getX(){
    return x;
}

int position::getY(){
    return y;
}

int position::getZ(){
    return z;
}
